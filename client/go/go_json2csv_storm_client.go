// package name: json2csv
package main

import (
    "C"
    "bytes"
    "encoding/csv"
    "encoding/json"
    "log"
    "net"
    "net/url"
    "regexp"
    "strconv"
    "strings"
    "sync"
    "syscall"
    "time"
    "fmt"
    "io/ioutil"
    "flag"
    "github.com/oschwald/geoip2-golang"
    "github.com/ua-parser/uap-go/uaparser"
)

const (
    BO_TmFmt = "2006-01-02 15:04:05"
    BO_TMPFILE = 0x410000

    GEO_DB_NAME = "GeoLite2-City.mmdb"
    UA_PARSER_YAML = "regexes.yaml"
)

type RegexpReferType int32

const (
    ReRefer_Smg RegexpReferType = iota
    ReRefer_FbM
    ReRefer_Fb1
    ReRefer_Fb2
    ReRefer_Fb3
    ReRefer_GoogleN
    ReRefer_Google1
    ReRefer_Google2
    ReRefer_Google3
    ReRefer_Google4
    ReRefer_Youtube
    ReRefer_YahooN
    ReRefer_YahooM
    ReRefer_Yahoo
    ReRefer_Msn
    ReRefer_Bing
    ReRefer_Wiki
    ReRefer_Twitter
    ReRefer_Bitly
    ReRefer_Jptt
    ReRefer_SonyN
    ReRefer_Flipboard
    ReRefer_NewsRepublic
    ReRefer_Pinterest
    ReRefer_Udn
    ReRefer_Baidu
    ReRefer_Cmoney
    ReRefer_Tumblr
    ReRefer_Meetedgar
    ReRefer_Buzzbooklet
    ReRefer_Getpocket
    ReRefer_Plurk
    ReRefer_Instagram
    ReRefer_Xuite
    ReRefer_Pixnet
    ReRefer_Upperbound
)

type RegexpUtmType int32

const (
    ReUtm_Yahoo RegexpUtmType = iota
    ReUtm_Ileopard
    ReUtm_Popin
    ReUtm_LineN
    ReUtm_Line
    ReUtm_Smg
    ReUtm_SmgCrm
    ReUtm_Fbia
    ReUtm_Fb1
    ReUtm_Fb2
    ReUtm_Fb3
    ReUtm_Pnn
    ReUtm_Upperbound
)

type RegexpClause struct {
    Source      string
    Clause      string
}

var (
    ReReferSources = []RegexpClause {
        {"storm_other", "^(.+\\.|)(storm\\.mg|stormmediagroup\\.com)$"},
        {"Facebook_mobi", "^[m]\\.facebook\\.com$"},
        {"Facebook", "^(.+\\.|)(facebook|messenger)\\.((com?\\.|)[a-z]{2}|com)$"},
        {"Facebook", "^fb\\.me$"},
        {"Facebook", "^com\\.facebook(|\\..+)$"},
        {"Google_News", "^(.+\\.|)news(\\.url|)\\.google\\.((com?\\.|)[a-z]{2}|com)$"},
        {"Google", "^(.+\\.|)google\\.((com?\\.|)[a-z]{2}|com)$"},
        {"Google", "^goo\\.gl$"},
        {"Google", "^com\\.google(|\\..+)$"},
        {"Google", "^(.+\\.|)google(apis|usercontent)\\.com$"},
        {"Youtube", "^(.+\\.|)youtube\\.((com?\\.|)[a-z]{2}|com)$"},
        {"Yahoo_News", "^(.+\\.|)news\\.yahoo\\.((com?\\.|)[a-z]{2}|com)$"},
        {"Yahoo_mobi", "^(.+\\.|)mobi.yahoo\\.((com?\\.|)[a-z]{2}|com)$"},
        {"Yahoo", "^(.+\\.|)yahoo\\.((com?\\.|)[a-z]{2}|com)$"},
        {"msn", "^(.+\\.|)msn\\.com$"},
        {"bing", "^(.+\\.|)bing\\.com$"},
        {"Wiki", "^(.+\\.|)wikipedia\\.org$"},
        {"Twitter", "^t\\.co$"},
        {"bitly", "^bit\\.ly$"},
        {"jptt", "^com\\.joshua\\.jptt$"},
        {"Sony_Newsuite", "^(.+\\.|)socialife\\.app\\.sony\\.jp$"},
        {"flipboard", "^(.+\\.|)flipboard\\.com$"},
        {"News_Republic", "^(.+\\.|)(newsrepublic|newscdn\\.newsrep)\\.net$"},
        {"pinterest", "^(.+\\.|)pinterest\\.com$"},
        {"udn", "^(.+\\.|)udn\\.com$"},
        {"baidu", "^(.+\\.|)baidu\\.com(\\.cn|)$"},
        {"cmoney", "^(.+\\.|)cmoney(\\.com|)\\.tw$"},
        {"tumblr", "^(.+\\.|)t(\\.|)umblr\\.com$"},
        {"meetedgar", "^(.+\\.|)meetedgar\\.com$"},
        {"buzzbooklet", "^(.+\\.|)buzzbooklet\\.com$"},
        {"getpocket", "^(.+\\.|)getpocket\\.com$"},
        {"plurk", "^(.+\\.|)plurk\\.com$"},
        {"instagram", "^(.+\\.|)instagram\\.com$"},
        {"xuite", "^(.+\\.|)xuite\\.net$"},
        {"pixnet", "^(.+\\.|)pixnet\\.net$"},
    }

    ReUtmSources = []RegexpClause {
        {"yahoo", "^YAHOO.*"},
        {"ileopard", "^ILEOPARD"},
        {"popin", "^POPIN"},
        {"line_news", "^LINE *新聞"},
        {"line", "^LINE"},
        {"storm.mg", "^STORM\\.MG.*"},
        {"storm.mg", "^STORM[0-9]*_CRM"},
        {"FBIA", "^FBIA"},
        {"Facebook", "^FACEBOOK"},
        {"Facebook", "^FB"},
        {"Facebook", "^XIN.*FB$"},
        {"pnn", "^PNN"},
    }
)

const (
    FilterTarget = "\\.(jpe?g|gif|png|js|css|ico|php|svg|woff|JPG|flv|eot|map|ttf|json)"
)

var (
    CompiledReferRegexp = make([]*regexp.Regexp, ReRefer_Upperbound)
    CompiledUtmRegexp = make([]*regexp.Regexp, ReUtm_Upperbound)
    CompiledFilter  *regexp.Regexp
)

var (
    once        sync.Once
    OutDir      string
    BOTz        *time.Location

    GeoDB       *geoip2.Reader
    UAParser    *uaparser.Parser
)

// These are output for C to avoid GC
var (
    out_err_ptr     []string
    out_tbl_ptr     [][]string
    out_fd_ptr      [][]int
)

var (
    SmgSources = map[string]string {
        "article": "storm_article",
        "articles": "storm_article",
        "lifestyles": "storm_lifestyle",
        "lifestyle": "storm_lifestyle",
        "category": "storm_category",
        "authors": "storm_author",
        "18-restricted": "storm_18-restricted",
        "feeds": "storm_feed",
        "localarticles": "storm_localarticle",
        "localarticle": "storm_localarticle",
        "localarticle-category": "storm_localarticle",
        "stylish-category": "storm_stylish",
        "stylish": "storm_stylish",
    }
)


/*
type UePr struct {
    Data    UePrData `json:"data"`
}

type UePrData struct {
    Data    UePrInfo `json:"data"`
}

type UePrInfo struct {
    Name    string `json:"name"`
    Id      string `json:"id"`
}

type AppCo struct {
    Data    []CoData `json:"data"`
}

type CoData struct {
    Data    CoInfo `json:"data"`
}

type CoInfo struct {
    NetType         string `json:"networkType"`
    OsType          string `json:"osType"`
    OsVer           string `json:"osVersion"`
    AppleId         string `json:"appleIdfv"`
    AndroidId       string `json:"androidIdfa"`
    Carrier         string `json:"carrier"`
    Manufacturer    string `json:"deviceManufacturer"`
    Model           string `json:"deviceModel"`
    PrevSessionId   string `json:"previousSessionId"`
    SessionId       string `json:"sessionId"`
    SessionIndex    int    `json:"sessionIndex"`
    UserId          string `json:"userId"`
    FirstEventId    string `json:"firstEventId"`

    SeqNo           string `json:"seqNo"`
    Action          string `json:"action"`
    Type            string `json:"type"`
    Aid             string `json:"aid"`
    Alg             string `json:"algorithm"`
}
*/
type CdnClientCommon struct {
    InputTs         string
    InputDateTime   string
    InputDate       string
    Cip             string
    Cookie_ga       string
    Cookie_smg      string
    UaDevice        string
    UaName          string
    UaOS            string
    HostHeader      string
    City            string
    CountryCode     string
    Country         string
    Latitude        string
    Longitude       string
    Tz              string
    ReqId           string
    Db_time         string
    Db_lagtime      string
}

//=========leo =========================
type config_struct struct{
	Versions	[]version_struct `json:versions`
    
}

type version_struct struct {
	Version string			`json:version`
	Time	q_h_struct		`json:time`
	Header  []q_h_struct	`json:header`
    Query   []q_h_struct	`json:query`
}

type q_h_struct struct{
    Input_Field_Name     string		`json:input_field_name`
    Input_Field_Type     string		`json:input_field_type`
    Output_Field_Name    string		`json:output_field_name`
    Output_Field_Type    string		`json:output_field_type`
    Parse       map[string]interface{}
}

//==========================
/* $$ 2019/07/05 Frank: delete "storm_data" structure
type storm_data_struct struct{
	data_array	[]data_array_struct	`json:storm_data`
    
}
*/
/*
type data_array_struct struct {
	Time    int64           `json:time`
    Header  header_struct   `json:header`
    Query   query_struct    `json:query`
    Version int             `json:version`
}
type header_struct struct{
    Host        string  `json:host`
    X_amzn_trace_id string  `json:x-amzn-trace-id`
    User_agent  string  `json:user-agent`
    Accept  string  `json:accept`
    Accept_encoding string  `json:accept-encoding`
    Accept_language string  `json:accept-language`
    X_forwarded_for string  `json:x-forwarded-for`
    X_forwarded_port    string  `json:x-forwarded-port`
    X_forwarded_proto   string  `json:x-forwarded-proto`
	Connection  string  `json:connection`
//	Referer		string	`json:referer`
}

type query_struct struct{
    Stm     string  `json:stm`
    E       string  `json:e`
    Url     string  `json:url`
    Page    string  `json:page`
    Pp_mix  string  `json:pp_mix`
    Pp_max  string  `json:pp_max`
    Pp_miy  string  `json:pp_miy`
    Pp_may  string  `json:pp_may`
    Tv      string  `json:tv`
    Tna     string  `json:tna`
	Aid     string  `json:aid`
	AppId	string	`json:appId`
    P       string  `json:p`
    Tz      string  `json:tz`
    Lang    string  `json:lang`
    Cs      string  `json:cs`
    F_pdf   string  `json:f_pdf`
    F_qt    string  `json:f_qt`
    F_realp string  `json:f_realp`
    F_wma   string  `json:f_wma`
    F_dir   string  `json:f_dir`
    F_fla   string  `json:f_fla`
    F_java  string  `json:f_java`
    F_gears string  `json:f_gears`
    Res     string  `json:res`
    Cd      string  `json:cd`
    Cookie  string  `json:cookie`
    Eid     string  `json:eid`
    Dtm     string  `json:dtm`
    Vp      string  `json:vp`
    Ds      string  `json:ds`
    Vid     string  `json:vid`
    Sid     string  `json:sid`
    Duid    string  `json:duid`
    Fp      string  `json:fp`
    Uid     string  `json:uid`
    Co      string  `json:co`
}
*/

type CdnClientOther struct {
	
    referer		string              //'referer'VARSTRING(200)
   
    target		string              //'target'VARSTRING(100)
    articleNid  string              //'articles.nid'INT64
    
    utmSrc      string              //'utm_source'STRING(100)
    bo_flag     string              //'bo_flag'INT8
    source      string              //'source'STRING(63)
    targett     string              //'targett'STRING(63)
    
    
    utm         string              //'utm'STRING(100)
    mid         string              //'member.mid'INT32
    stm         string              //'stm'DATETIME32
    event       string              //'event'STRING(3)
    pp_mix      string              //'pp_mix'INT32
    pp_max      string              //'pp_max'INT32
    pp_miy      string              //'pp_miy'INT32
    pp_may      string              //'pp_may'INT32
    platform    string              //'platform'STRING(5)
    screen_w    string              //'screen_w'INT32
    screen_h    string              //'screen_h'INT32
    eid         string              //'eid'VARSTRING(100)
	dtm         string              //'dtm'DATETIME32
	viewport_w  string              //'viewport_w'INT32
    viewport_h  string              //'viewport_h'INT32
    page_w      string              //'page_w'INT32
    page_h      string              //'page_h'INT32
    vid         string              //'vid'INT8
    sid         string              //'session_id'VARSTRING(100)
    duid        string              //'duid'VARSTRING(100)
    fp          string              //'fp'INT64
    fp2         string              //'fp2'INT64
    fp3         string              //'fp3'INT64
    uid         string              //'uid'INT64
    scroll_st   string              //'scroll'INT32   //Math.floor(pp_may/(page_h - viewport_h) * 100)

	ArticlesTitle	string
}

//export SetCounter
func SetCounter( string,  int) {
}

//export SetConfig
func SetConfig(_ string) {
}

//=========leo ======================

func IsFiltered(target string) bool {
    return CompiledFilter.MatchString(target)
}

func getHostName(hostport string) string {
    colon := strings.IndexByte(hostport, ':')
    if colon == -1 {
        return hostport
    }
    if i := strings.IndexByte(hostport, ']'); i != -1 {
        return strings.TrimPrefix(hostport[:i], "[")
    }
    return hostport[:colon]
}

func ParseReferer(data string) (referer, source, utmSrc string) {
    
    if len(data) == 0 || data == "-" {
        referer = ""
        source = "DIRECT"
        utmSrc = ""
        return
    }

    referer = data

    u, err := url.Parse(data)
    if err != nil {
        log.Printf("Parse url (%s) fail: %v\n", data, err)
        return
    }

    q := u.Query()
    utmSrc = q.Get("utm_source")

    uriDir1 := ""
    hostName := getHostName(u.Host)
    uri := u.RequestURI()
    if uriPath := strings.SplitN(uri, "?", 2); len(uriPath) > 0 {
        if uriDir := strings.SplitN(uriPath[0], "/", 3); len(uriDir) >= 2 {
            uriDir1 = uriDir[1]
        }
    }

    if strings.HasPrefix(data, "android-app://") {
        if strings.HasPrefix(data, "android-app://m.facebook.com") {
            source = "Facebook_mobi"
            return
        }

        for i, re := range CompiledReferRegexp {
            if re.MatchString(hostName) {
                source = fmt.Sprintf("android-app_%s", ReReferSources[i].Source)
                break
            }
        }
        if len(source) == 0 {
            source = "android-app"
        }
        return
    }

    if !strings.HasPrefix(data, "http://") && !strings.HasPrefix(data, "https://") {
        source = "NOT_A_VALID_HOSTNAME"
        return
    }

    switch hostName {
    case "www.storm.mg", "www.stormmediagroup.com", "storm.mg", "events.storm.mg":
        if len(uriDir1) == 0 {
            source = "storm.mg"
        } else {
            var ok bool
            if source, ok = SmgSources[uriDir1]; !ok {
                source = "storm_other"
            }
        }
    default:
        for i, re := range CompiledReferRegexp {
            if re.MatchString(hostName) {
                source = ReReferSources[i].Source
                break
            }
        }
        if len(source) == 0 {
            source = hostName
        }
    }
    return referer, source, utmSrc
}

func ParseUtm(utmSrc string) (utm string) {
    if len(utmSrc) == 0 || utmSrc == "null" || utmSrc[0] == ' ' {
        utm = ""
        return utm
    }

    data := strings.ToUpper(utmSrc)
    for i, re := range CompiledUtmRegexp {
        if re.MatchString(data) {
            utm = ReUtmSources[i].Source
            break
        }
    }
    if len(utm) == 0 {
        utm = "other"
    }
    return utm
}

func ParseTarget(data string) (tgt, article string) {
    target_url, err := url.Parse(data)
    if err != nil {
        log.Printf("Invalid target url (%s): %v\n", data, err)
        tgt = "storm.mg"
        return
    }
    tgtData := strings.SplitN(strings.TrimLeft(target_url.RequestURI(), "/"), "/", 3)
    entNum := len(tgtData)
    if entNum == 0 {
        tgt = "storm.mg"
        return
    }

    switch tgtData[0] {
    case "article":
        tgt = "article"
        if entNum > 1 {
            article = tgtData[1]
        }
    case "articles", "article-page":
        tgt = "article"
    case "authors":
        tgt = "author"
    case "category":
        tgt = "category"
    case "lifestyle":
        tgt = "lifestyle"
        if entNum > 1 {
            article = tgtData[1]
        }
    case "lifestyles", "lifestyle-page", "life-category":
        tgt = "lifestyle"
    case "feeds", "feed-single":
        tgt = "feed"
    case "feed":
        if entNum >= 2 && entNum > 1 && tgtData[1] == "author" {
            tgt = "author"
            // if entNum > 2 {
            //     article = tgtData[2]
            // } else {
            //     article = "-"
            // }
        } else {
            tgt = "feed"
        }
    case "stylish":
        tgt = "stylish"
        if entNum > 1 {
            article = tgtData[1]
        }
    case "stylish-category", "stylish-page":
        tgt = "stylish"
    case "localarticle":
        tgt = "localarticle"
        if entNum > 1 {
            article = tgtData[1]
        }
    case "localarticles", "localarticle-category", "localarticle-page":
        tgt = "localarticle"
    default:
        tgt = "other"
    }
    // if entNum >= 2 && len(article) == 0 {
    //     article = tgtData[1]
    // } else if article == "-" {
    //     article = ""
    // }
    return tgt, article
}
func WriteToFile(records [][]string) (fd int, err error) {

    if fd, err = syscall.Open(OutDir, syscall.O_RDWR|BO_TMPFILE|syscall.O_EXCL|syscall.O_CLOEXEC, 0666); err != nil {
        return fd, err
    }

    wBuffer := new(bytes.Buffer)
    wBuffer.Grow(8196)
    w := csv.NewWriter(wBuffer)
    w.WriteAll(records)
    if err := w.Error(); err != nil {
        syscall.Close(fd)
        fd = -1
        return fd, err
    }

    syscall.Write(fd, wBuffer.Bytes())
    return fd, nil
}


func TranslateCookiemap(common *CdnClientCommon,cookieurl string) []string {
    url, _ := url.PathUnescape(cookieurl)
    //cookie_data := uriValues.Get("data")


    log := make([]string, 20)           // <<--- public_cookie 記得改回來20
    log[0]  = common.InputTs            //'ts'TIMESTAMP,
    log[1]  = common.Cip                //'cip'IPv6,
    log[2]  = common.Cookie_ga          //'cookiega'VARSTRING(30),
    log[3]  = common.Cookie_smg         //'cookieuid'INT64,
    log[4]  = common.InputDateTime      //'inputdate'DATETIME32,
    log[5]  = url                       //'url'VARSTRING(200),
    log[6]  = common.UaDevice           //'agentdevice'STRING(50),
    log[7]  = common.UaName             //'agentname'STRING(30),
    log[8]  = common.UaOS               //'agentosname'STRING(15),
    log[9]  = common.HostHeader         //'xhostheader'CHAR(30),
    log[10] = common.City               //'ipcityname'STRING(64),
    log[11] = common.CountryCode        //'ipcountrycode2'STRING(3),
    log[12] = common.Country            //'ipcountryname'STRING(64),
    log[13] = common.Latitude           //'iplatitude'FLOAT,
    log[14] = common.Longitude          //'iplongitude'FLOAT,
    log[15] = common.Tz                 //'iptimezone'CHAR(30),
    log[16] = common.ReqId              //'requestid'VARSTRING(300),
    log[17] = common.InputDate          //'date'CHAR(11),
    log[18] = common.Db_time
    log[19] = common.Db_lagtime

//    log[20] = public_cookie
    //log[18] = cookie_data               //'data'VARSTRING(500)

    return log
}




func TranslateLog (common *CdnClientCommon, other *CdnClientOther) ([] string){
	
	

    log := make([]string, 52) // <<--- public_cookie 記得改回來
    log[0]  = common.InputTs            //'ts'TIMESTAMP
    log[1]  = common.Cip                //'cip'IPv6
    log[2]  = common.Cookie_ga          //'cookiega'VARSTRING(30)
    log[3]  = common.Cookie_smg         //'cookieuid'INT64
    log[4]  = common.InputDateTime      //'inputdate'DATETIME32
    log[5]  = other.referer                   //'referer'VARSTRING(200)
    log[6]  = common.UaDevice           //'agentdevice'STRING(50)
    log[7]  = common.UaName             //'agentname'STRING(30)
    log[8]  = common.UaOS               //'agentosname'STRING(15)
    log[9]  = common.HostHeader         //'xhostheader'CHAR(30)
    log[10] = common.City               //'ipcityname'STRING(64)
    log[11] = common.CountryCode        //'ipcountrycode2'STRING(3)
    log[12] = common.Country            //'ipcountryname'STRING(64)
    log[13] = common.Latitude           //'iplatitude'FLOAT
    log[14] = common.Longitude          //'iplongitude'FLOAT
    log[15] = common.Tz                 //'iptimezone'CHAR(30)
    log[16] = other.target                    //'target'VARSTRING(100)
    log[17] = other.articleNid                //'articles.nid'INT64
    log[18] = common.ReqId              //'requestid'VARSTRING(300)
    log[19] = other.utmSrc                    //'utm_source'STRING(100)
    log[20] = other.bo_flag                   //'bo_flag'INT8
    log[21] = other.source                    //'source'STRING(63)
    log[22] = other.targett                   //'targett'STRING(63)
    log[23] = ""                        //'tag'STRING(63)
    log[24] = common.InputDate          //'date'CHAR(11)
    log[25] = other.utm                       //'utm'STRING(100)
    log[26] = other.mid                       //'member.mid'INT32
    log[27] = other.stm                       //'stm'DATETIME32
    log[28] = other.event                     //'event'STRING(3)
    log[29] = other.pp_mix                    //'pp_mix'INT32
    log[30] = other.pp_max                    //'pp_max'INT32
    log[31] = other.pp_miy                    //'pp_miy'INT32
    log[32] = other.pp_may                    //'pp_may'INT32
    log[33] = other.platform                  //'platform'STRING(5)
    log[34] = other.screen_w                  //'screen_w'INT32
    log[35] = other.screen_h                  //'screen_h'INT32
    log[36] = other.eid                       //'eid'VARSTRING(100)
    log[37] = other.dtm                       //'dtm'DATETIME32
    log[38] = other.viewport_w                //'viewport_w'INT32
    log[39] = other.viewport_h                //'viewport_h'INT32
    log[40] = other.page_w                    //'page_w'INT32
    log[41] = other.page_h                    //'page_h'INT32
    log[42] = other.vid                       //'vid'INT8
    log[43] = other.sid                       //'session_id'VARSTRING(100)
    log[44] = other.duid                      //'duid'VARSTRING(100)
    log[45] = other.fp                        //'fp'INT64
    log[46] = other.fp2                       //'fp2'INT64
    log[47] = other.fp3                       //'fp3'INT64
    log[48] = other.uid                       //'uid'INT64
    log[49] = other.scroll_st                 //'scroll'INT32   //Math.floor(pp_may/(page_h - viewport_h) * 100)
    
    log[50] = common.Db_time
    log[51] = common.Db_lagtime
//    log[52] = public_cookie
    return log


}

// AWS CDNLOG format
//  0: date, 2015-06-30
//  1: time in UTC, 01:42:39
//  2: x-edge-location, "TPE50"
//  3: sc-bytes, 12345
//  4: c-ip, client ip address, 60.248.178.121
//  5: cs-method, "GET"
//  6: cs(Host), "d11xfkicikqxp2.cloudfront.net"
//  7: cs-uri-stem, "/assets/footer_logo.png" or "/article/300128"
//  8: sc-status, 200
//  9: cs(Referer), "http://www.storm.mg/" or "http://www.storm.mg/article/166531?utm_source=Yahoo&utm_medium=%25E7%259B%25B8%25E9%2597%259C%25E5%25A0%25B1%25E5%25B"
// 10: cs(User-Agent), "Mozilla/5.0%2520(iPhone;%2520CPU%2520iPhone%2520OS%252010_3_2%2520like%2520Mac%2520OS%2520X)%2520AppleWebKit/603.2.4%2520(KHTML,%2520like%2520Gecko)%2520Version/10.0%2520Mobile/14F89%2520Safari/602.1"
// 11: cs-uri-query, "v=7a10f21"
// 12: cs(Cookie), "smg_uid=1492646199415499;%2520_td=7c8c0736-7f9c-41bb-9d6d-"
// 13: x-edge-result-type, "Hit" or "Miss"
// 14: x-edge-request-id, "tdSJJ9Wt8wVwFO9aAm0mR-BqZGL0rs89OVh_Ereu-lpJAeywDSeJJA=="
// 15: x-host-header, "www.storm.mg"
// 16: cs-protocol, "http" or "https"
// 17: cs-bytes, 1234
// 18: time-taken, 0.1234
// 19: x-forwarded-for, proxy only, -
// 20: ssl-protocol, "TLSv1.2"
// 21: ssl-cipher, "ECDHE-RSA-AES128-GCM-SHA256"
// 22: x-edge-response-result-type, "Hit" or "Miss"
// 23: cs-protocol-version, HTTP/1.1

func TranslateCdn(data_map map[string]interface{}, fatal_error *string) (fdCdn, fdRobot, fdCookie, fdCookieRobot int) {
	
	fdCdn = -1
    fdRobot = -1
    fdCookie = -1
    fdCookieRobot = -1

    logRecords := [][]string{}
    logrobotRecords := [][]string{}
    cookieRecords := [][]string{}
	cookierobotRecords := [][]string{}
	
	// $$ turn 
//	fmt.Println("====================================\n", "Time", ": ", storm_struct["time"])
//	fmt.Println(version_conf.Time)

	/*
		$$ don't know how to parse:
        
        x-forwarded-proto
        x-forwarded-port
        accept
        accept-encoding
        accept-language
        x-requested-with
        
        $$ parse but not put it the table:
    
        Articles.title

        //======================
        
		if len(row) < 24 {
            log.Println("Invalid row data:", row)
            continue
		}
		if row[8] != "200" { // http response status
            continue
		}
		if IsFiltered(row[7]) {
            continue
        }
        if row[7] != "/i" && row[7] != "/storm/i" {
            continue
		}
		
		
		
	*/
//	fmt.Println(data_map["storm_data"].([]interface{})[1].(map[string]interface{})["version"] )
  //  fmt.Println("data_map: ", data_map)

    /*
    ==========================================
        $$ 2019/07/05 Frank: delete "storm_data" structure

    var data_array_raw []interface{}
    if v, found := data_map["storm_data"]; found {
        data_array_raw = v.([]interface{})

        for _, v := range data_array_raw {
     ============================================
    */
    //        storm_struct := v.(map[string]interface{}) $$ 2019/07/05 Frank: delete "storm_data" structure
            storm_struct := data_map
   
    //    	fmt.Println(storm_struct)
    
    //==============================================================
    //		$$ Leo: reading different version of config actually has no use now,
    //		$$		but I did it anyway. It should be improved in future version of streamer
    //		current_version := int(storm_struct["version"].(float64))
            
    //		var version_conf version_struct 
    //		version_conf = Read_json_config("storm_json_config.json", current_version)
    
    //        compose_config_map(version_conf)
    //        fmt.Println("compose_config_map complete")
    //=====================================================
    
        //    header_conf_map := make(map[int] map[string]int)
        //    query_conf_map := make(map[int] map[string]int)
            

            header_map := storm_struct["header"].(map[string]interface{})
            query_map := storm_struct["query"].(map[string]interface{})

            // $$ ============path, aid, appid=============
            

        //    temp_appid := return_if_map_exist(query_map, "appId")
        //    temp_aid := return_if_map_exist(query_map, "aid")
        
            data_type := ""
            // $$   client  --> path = "/i"     --> aid = "5c6e1a"
            //              --> path = "/storm/i" --> appid = "5c6e1a"
            if temp_path, found := storm_struct["path"].(string); found {

            // $$ has path, means it is client
                switch temp_path {
                case "/i":
                    if InterfaceToStr(query_map["aid"]) != "5c6e1a"  {
                        temp_msg := "Invalid aid: " + InterfaceToStr(query_map["aid"])
                        *fatal_error = temp_msg
                        return// continue $$ 2019/07/05 Frank: delete "storm_data" structure
                    } else {
                        data_type = "/i"
                    }
                case "/storm/i":
                    if InterfaceToStr(query_map["appId"]) != "5c6e1a" {
                        temp_msg := "Invalid appId: " + InterfaceToStr(query_map["appId"])
                        *fatal_error = temp_msg
                        return// continue $$ 2019/07/05 Frank: delete "storm_data" structure
                    } else {
                        data_type = "/storm/i"
                    }
                        
                default:
                    return // continue $$ 2019/07/05 Frank: delete "storm_data" structure // $$ not valid path, skip it and procced to next records // 
                }
            } else {
                temp_msg := "path missing"
                *fatal_error = temp_msg
                fmt.Println(temp_msg) 
                return
            }

            temp_host := InterfaceToStr(header_map["host"])
            if temp_host != "prod-src-track.storm.mg" && temp_host != "track.storm.mg" {
                temp_msg := "Invalid host: " + temp_host
                *fatal_error = temp_msg
                log.Println(temp_msg)
                return   
            }
            
            // $$ ============header============================
            
    
        //    fmt.Println("header_map: ", header_map)
            // $$ ============query=====================
        //    fmt.Println("query_map: ", query_map)
           

            var common CdnClientCommon
            var other CdnClientOther
    
            common = make_common(storm_struct)
            
            other = make_other(storm_struct, &common, data_type)

        //====================================
       //     if appId := return_if_map_exist(query_map, "appId");len(appId) > 0 {
           
         //   fmt.Println("data_type: ", data_type)
        //    fmt.Println("common: ", common)
        //    fmt.Println("other: ", other)
           
           
            if data_type == "/storm/i" {   
                record := TranslateCookiemap(&common ,InterfaceToStr(query_map["refr"]))
                if common.UaDevice == "Spider" {
                    cookierobotRecords = append(cookierobotRecords, [][]string{record}...)
                } else {
                    cookieRecords = append(cookieRecords, [][]string{record}...)
                } 
            } else{
                
                record := TranslateLog(&common, &other)
                if common.UaDevice == "Spider" {
                    logrobotRecords = append(logrobotRecords, [][]string{record}...)
                } else {
                    
                    logRecords = append(logRecords, [][]string{record}...)
                }
            }
            
        /*
        //		$$ Leo: reading different version of config actually has no use now,
        //		$$		but I did it anyway. It should be improved in future version of streamer
    
            version := string_to_int(version_conf.Version)
            for key, value := range storm_struct["header"].(map[string]interface{}) {
                fmt.Println("====================================\n", key, ": ", value, ": ", header_conf_map[version][key])
                fmt.Println(version_conf.Header[header_conf_map[version][key]])
            }
    
            for key, value := range storm_struct["query"].(map[string]interface{}) {
                fmt.Println("====================================\n", key, ": ", value, ": ", query_conf_map[version][key])
                fmt.Println(version_conf.Query[query_conf_map[version][key]])
            }
            */
            
    //    }  $$ 2019/07/05 Frank: delete "storm_data" structure
    
    //	fmt.Println("cookierobotRecords: ", cookierobotRecords)
    //	fmt.Println("cookieRecords: ", cookieRecords)
   // 	fmt.Println("logrobotRecords: ", logrobotRecords)
   // 	fmt.Println("logRecords: ", logRecords)
        var err error
        if len(logRecords) > 0 {
            if fdCdn, err = WriteToFile(logRecords); err != nil {
                log.Println("write cdnlog_client data fail:", err)
            }
        }
        if len(logrobotRecords) > 0 {
            if fdRobot, err = WriteToFile(logrobotRecords); err != nil {
                log.Println("write cdn_robot_client data fail:", err)
            }
        }
        if len(cookieRecords) > 0 {
            if fdCookie, err = WriteToFile(cookieRecords); err != nil {
                log.Println("write cookiemap_cdnlog_client data fail:", err)
            }
        }
        if len(cookierobotRecords) > 0 {
            if fdCookieRobot, err = WriteToFile(cookierobotRecords); err != nil {
                log.Println("write cookieuid_cdn_robot_client data fail:", err)
            }
        }
        
        
//    } $$ 2019/07/05 Frank: delete "storm_data" structure
    
//    fmt.Println(data_map)
//	data_array_raw := data_map["storm_data"].([]interface{})

	
//    fmt.Println(fdCdn, ", ", fdRobot, ", ", fdCookie, ", ", fdCookieRobot)
    return fdCdn, fdRobot, fdCookie, fdCookieRobot
	

}



//export JsonToCsv
func JsonToCsv(inFd []int, outPath string) (out_tbl [][]string, out_fd [][]int, err_str []string) {
    once.Do(func() { // $$ make sure it only done once
        var err error

        if BOTz, err = time.LoadLocation("Asia/Taipei"); err != nil {
            log.Fatalln("Load TW time zone fail:", err)
        }

        if GeoDB, err = geoip2.Open("../common/" + GEO_DB_NAME); err != nil {
            log.Fatalln("Open geo db fail:", err)
        }

        if UAParser, err = uaparser.New("../common/" + UA_PARSER_YAML); err != nil {
            log.Fatal(err)
        }

        // Compiled regexp
        if len(CompiledReferRegexp) != len(ReReferSources) {
            log.Fatalln("Referer regexp structure mismatch")
        }
        for i, re := range ReReferSources {
            if CompiledReferRegexp[i], err = regexp.Compile(re.Clause); err != nil {
                log.Fatalf("Compile \"%s\" fail: %v\n", re.Clause, err)
            }
        }

        if len(CompiledUtmRegexp) != len(ReUtmSources) {
            log.Fatalln("Utm regexp structure mismatch")
        }
        for i, re := range ReUtmSources {
            if CompiledUtmRegexp[i], err = regexp.Compile(re.Clause); err != nil {
                log.Fatalf("Compile \"%s\" fail: %v\n", re.Clause, err)
            }
        }

        if CompiledFilter, err = regexp.Compile(FilterTarget); err != nil {
            log.Fatalf("Compile filter regexp fail: %v\n", err)
        }

        OutDir = outPath
    })

    var wg sync.WaitGroup // $$ make sure it wait until all goroutines are finished
    err_str = make([]string, len(inFd))
    out_tbl = make([][]string, len(inFd))
    out_fd  = make([][]int, len(inFd))

    for idx, jFd := range inFd {
        // $$ launch a goroutine
        wg.Add(1)
        go func(idx, jFd int) {
            defer func() {
                wg.Done()
            }()
            
            
            //===============================================
            var inStat syscall.Stat_t
            if err := syscall.Fstat(jFd, &inStat); err != nil {
                fmt.Printf("Get input file state fail: %v\n", err)
                err_str[idx] = err.Error()
            //   return
                return
            }

            jsonData, err := syscall.Mmap(jFd, 0, int(inStat.Size), syscall.PROT_READ, syscall.MAP_SHARED)
            if err != nil {
                fmt.Printf("Mmap input file fail: %v\n", err)
                err_str[idx] = err.Error()
                return
            }
            defer syscall.Munmap(jsonData)
            
            /*
            //=====================================================
            r := csv.NewReader(bytes.NewReader(jsonData)) 
            r.Comma = '\t' // $$ 設定分隔字元
            r.Comment = '#' // $$ 設定註解字元
            records, err := r.ReadAll() // $$ 一次讀完整個csv
            if err != nil {
                log.Println("Read csv fail:", err)
                err_str[idx] = err.Error()
                return
            }
            // $$ 讀csv的資料丟進 TranslateCdn
            // $$ 根據output決定要丟哪個table
            fdCdn, fdRobot, fdCookie, fdCookieRobot := TranslateCdn(records)
            //===================================
            */

            //===========================
            jsonData = bytes.Replace(jsonData, []byte("NaN"), []byte("0"), -1)
            
            var data_map map[string]interface{}
            
         //   json.Unmarshal([]byte(jsonData), &data_map)
            
            fdCdn := -1
            fdRobot := -1
            fdCookie := -1
            fdCookieRobot := -1

            var fatal_error string

        //    fmt.Println("json: ", string(jsonData))
            if err = json.Unmarshal([]byte(jsonData), &data_map); err == nil && len(data_map) > 0 {
            //    public_cookie := parsecustom(data_map)
                fdCdn, fdRobot, fdCookie, fdCookieRobot = TranslateCdn(data_map, &fatal_error)
                err_str[idx] = fatal_error
            //    fmt.Println("TranslateCdn, idx: ", idx, ": ", fdCdn, ", ", fdRobot, ", ", fdCookie, ", ", fdCookieRobot )
            } else {
                fatal_error = "Parse error: " + err.Error()
                fmt.Println(fatal_error)
                err_str[idx] = fatal_error
                
            }

            
            
            //=====================================
            out_tbl[idx] = []string{}
            out_fd[idx] = []int{}

            if fdCdn > 0 {
                out_tbl[idx] = append(out_tbl[idx], []string{"cdnlog_client"}...)
                out_fd[idx] = append(out_fd[idx], []int{fdCdn}...)
            }
            if fdRobot > 0 {
                out_tbl[idx] = append(out_tbl[idx], []string{"cdn_robot_client"}...)
                out_fd[idx] = append(out_fd[idx], []int{fdRobot}...)
            }
            if fdCookie > 0 {
                out_tbl[idx] = append(out_tbl[idx], []string{"cookiemap_cdnlog_client"}...)
                out_fd[idx] = append(out_fd[idx], []int{fdCookie}...)
            }
            if fdCookieRobot > 0 {
                out_tbl[idx] = append(out_tbl[idx], []string{"cookieuid_cdn_robot_client"}...)
                out_fd[idx] = append(out_fd[idx], []int{fdCookieRobot}...)
            }
        }(idx, jFd)
    }
    wg.Wait()

    // keep last output reference to avoid GC after return to C program
    out_err_ptr = err_str
    out_tbl_ptr = out_tbl
    out_fd_ptr  = out_fd
    
//    fmt.Println("out_tbl: ", out_tbl)
//    fmt.Println("out_fd: ", out_fd)
//    fmt.Println("err_str: ", err_str)
    return out_tbl, out_fd, err_str
}



func Read_json_config(source string, version int) (output version_struct) {
	
	// $$ read json_config
//	config_file := flag.String(source)
	json_reader, err := ioutil.ReadFile(source)
	if err != nil{
		log.Fatalln(err)
	}  
	
	var all_version_struct config_struct
	json.Unmarshal([]byte(json_reader), &all_version_struct)

	for _, value := range all_version_struct.Versions {
		if(string_to_int(value.Version) == version) {
			fmt.Println(version, " version config is found")
			return value // $$ 
		}
	}

	log.Fatal("no such veriosn config: ", version)
	return
}

func string_to_int(input string) (output int) {

	int_value, err := strconv.Atoi(input)
	if err != nil {
		log.Fatal("string convert failed")
	}
	return int_value
} 

func json_data_to_map(source string) (test_map map[string]interface{}) {

	config_file := flag.String("storm_data",source,"storm data")
	json_reader, err := ioutil.ReadFile(*config_file)
	if err != nil{
		log.Fatal(err)
	}  
	
	json.Unmarshal([]byte(json_reader), &test_map)
	return test_map
}

/*
func compose_config_map(version_conf version_struct) {

//	fmt.Println(version_conf.Header)
	version := string_to_int(version_conf.Version)

	// $$ build header map
	if v, found := header_conf_map[version]; found {
		fmt.Println(v, " exits in header map, do nothing")
	} else {
		// $$ build a map for header config
		header_conf_map[version] = make(map[string]int)
		for index, config_in_header := range version_conf.Header {
			header_conf_map[version][config_in_header.Input_Field_Name] = index
		}
	}

	// $$ build query map
	if v, found := query_conf_map[version]; found {
		fmt.Println(v, " exits in query map, do nothing")
	} else {
		// $$ build a map for header config
		query_conf_map[version] = make(map[string]int)
		for index, config_in_query := range version_conf.Query {
			query_conf_map[version][config_in_query.Input_Field_Name] = index
		}
	}

}
*/

/*
func return_if_map_exist(input map[string]interface{}, key string) (output string) {
	output = ""
	if v, found := input[key]; found {
		output = InterfaceToStr(v)
	}
	return output
}
*/

func convert_unixtime_to_time(input interface{}, format string) (output time.Time) {

    // $$ input: 1562137224094
    // $$ output: 2019-07-03 07:00:24.094 +0000 UTC
	var err error
	var unixtime_int64 int64
	
	switch input.(type) {
	case int64:
		unixtime_int64 = input.(int64)
	case int:
		unixtime_int64 = int64(input.(int))
	case string:
		unixtime_int64, err = strconv.ParseInt(input.(string), 10, 64)
	case float64:
		unixtime_int64 = int64(input.(float64))
	default:
		fmt.Println("InterFace type convert to int64 error,\n",input ,", ", err)
		unixtime_int64 = 0
	
	}	
	
	var exponent int64 // float64
	switch format {
	case "sec":
		exponent = 1000000000 // 9
	case "mili-sec":
		exponent = 1000000// 6
	case "nano-sec":
		exponent =  1// 0
	}
	
	nano_time_stamp := unixtime_int64 * exponent // int64(math.Pow(10, exponent))

	UnixTime_time := time.Unix(0, nano_time_stamp) // $$ convert nano-second to time format

    return UnixTime_time // $$ output looks like 2019-07-03 07:00:24.094 +0000 UTC
}

func convert_unixtime_to_datetime (input interface{}, format string) (output string) {
    UnixTime_time := convert_unixtime_to_time(input, format)
    // $$ turn 1561715875951 into YYYY-MM-DD hh:mm:ss
    output = UnixTime_time.In(BOTz).Format(BO_TmFmt)
    return output
}

func make_common(storm_struct map[string]interface{}) (common CdnClientCommon) {

//    var common CdnClientCommon
    var err error
    var ok bool
    header_map := storm_struct["header"].(map[string]interface{})

    query_map := storm_struct["query"].(map[string]interface{})

    // $$ ========== time===========
    /* $$
    input: 
        time
    output:
        common.InputTs
        common.InputDateTime
        common.InputDate

        common.Db_time
        common.Db_lagtime
    */
    UnixTime_time := convert_unixtime_to_time(storm_struct["time"], "mili-sec")	// $$ time stamp
    
 
    current_unix_10 := strconv.FormatInt(time.Now().UTC().Unix(), 10) // $$ 10 digits(sec), ex: 1557998531388

    common.Db_time = convert_unixtime_to_datetime(current_unix_10, "sec") // $$ YYYY-MM-DD hh:mm:ss
        
    // $$ common.InputTs want 10 digits, but storm_struct["time"] is 13 digits 
    common.InputTs = strconv.FormatInt(UnixTime_time.Unix(), 10) // 1557998531388

    var temp_InputTs int64
    var temp_current int64
    temp_InputTs, err = strconv.ParseInt(common.InputTs, 10, 64)
    temp_current, err = strconv.ParseInt(current_unix_10, 10, 64)
    common.Db_lagtime = strconv.FormatInt(temp_current - temp_InputTs, 10) // $$ get lag time in seconds

//    fmt.Println("common.Db_lagtime: ", common.Db_lagtime, "common.Db_time: ", common.Db_time)
    
    // $$ change to CST

//    tCst := UnixTime_time.In(BOTz)
//    common.InputDateTime = tCst.Format(BO_TmFmt) // 2019-05-16 17:22:11

    common.InputDateTime = convert_unixtime_to_datetime(storm_struct["time"], "mili-sec")	
    common.InputDate = strings.Split(common.InputDateTime," ")[0] // 2019-05-16
    
    

    // $$ =========host===============
    /* $$

    prod-src-track.storm.mg --> track.storm.mg

    input: 
        host
    output:
        common.HostHeader
    */

    common.HostHeader = strings.TrimPrefix(InterfaceToStr(header_map["host"]), "prod-src-")
//    common.HostHeader = header_map["host"].(string)

    // $$ =========parse: x-amzn-trace-id ===============
    /* $$
    input: 
        x-amzn-trace-id
    output:
        common.ReqId
    */
    common.ReqId = InterfaceToStr(header_map["x-amzn-trace-id"])

   


    
    // $$ ========x-forwarded-for==============
    /* $$
    input: 
        x-forwarded-for
    output:
        common.Cip
        common.Latitude
        common.Longitude
        common.Tz
        common.City
        common.Country
        common.CountryCode
    */
    // $$ "114.25.65.23, 52.46.62.68", get the first IP
    temp_ip_array := strings.Split(InterfaceToStr(header_map["x-forwarded-for"]), ",")
    common.Cip = strings.TrimSpace(temp_ip_array[0])


    city := "unknown"
    country := "unknown"
    countryCode := "unknown"
    ip := net.ParseIP(common.Cip)
    if geoCity, temp_err := GeoDB.City(ip); temp_err == nil {
        countryCode = geoCity.Country.IsoCode
        if city, ok = geoCity.City.Names["en"]; !ok {
            city = "unknown"
        }
        if country, ok = geoCity.Country.Names["en"]; !ok {
            country = "unknown"
            countryCode = "unknown"
        }


        common.Latitude = strconv.FormatFloat(geoCity.Location.Latitude, 'f', -1, 32)
        common.Longitude = strconv.FormatFloat(geoCity.Location.Longitude, 'f', -1, 32)
        common.Tz = geoCity.Location.TimeZone
        // continentCode = geoCity.Continent.Code
    }

    common.City = city
    common.Country = country
    common.CountryCode = countryCode
    

    

//    fmt.Println("get_agent_from_cookie: ", get_agent_from_cookie)
    // $$ ==========cookie==========================
    /* $$
    input: 
        cookie
    output:
        common.Cookie_ga
        common.Cookie_smg   
    */
    // parse Cookies
    var temp_cookie string
    if temp_cookie = InterfaceToStr(header_map["cookie"]); temp_cookie == "" {
        temp_cookie = InterfaceToStr(query_map["cookie"])
    }
    
    var temp_cookieData string
    cookieKv := make(map[string]string)
    if cookieDataTmp, temp_err := url.QueryUnescape(temp_cookie); temp_err == nil {
        if cookieData, temp_err := url.QueryUnescape(cookieDataTmp); temp_err == nil {
            //==============================
            temp_cookieData = cookieData // $$ make a copy for later use (user-agent)
        //    fmt.Println("cookieData: ", cookieData)
            
            //=================================
            cookies := strings.Split(cookieData, "; ")
        
            for _, aCookie := range cookies {
                kv := strings.SplitN(aCookie, "=", 2)
                if len(kv) == 2 {
                    
                    cookieKv[kv[0]] = kv[1]
                }
            }
        }
    }

    // get cookies we are interested  
    common.Cookie_ga, ok = cookieKv["_ga"]
    if !ok {
        common.Cookie_ga = ""
    }
    common.Cookie_smg, ok = cookieKv["smg_uid"]
    if !ok {
        common.Cookie_smg = "0"
    } else {
        if _, err = strconv.ParseInt(common.Cookie_smg, 10, 64); err != nil { // it's an array.
            uids := []int64{}
            if err = json.Unmarshal([]byte(common.Cookie_smg), &uids); err == nil && len(uids) > 0 {
                common.Cookie_smg = strconv.FormatInt(uids[0], 10)
            }
        }
    }
    
    //    fmt.Println("cookie:", temp_cookie)
    //    fmt.Println("cookieKv: ", cookieKv)
    //    fmt.Println("smg_a: ", cookieKv["smg_a"])

//    fmt.Println("temp_user_agent: ", temp_user_agent)

    // $$ ==========user-agent==========================
    /* $$
    input: 
        user-agent or cookie --> smg_a --> agent
    output:
        common.UaName
        common.UaDevice
        common.UaOS
    */
    var temp_user_agent string
    
    temp_user_agent = strings.TrimSpace(InterfaceToStr(header_map["user-agent"]))
    if temp_user_agent == "" || temp_user_agent == "Amazon CloudFront" {
    //    fmt.Println("temp_cookieData: ", temp_cookieData)
        // $$ $$ deal with "XXX; smg_a" or " smg_a"
        r, _ := regexp.Compile("([^ \f\n\r\t\v]+;)*[ \f\n\r\t\v]+smg_a=({.*});")

        var get_from_first bool = false
        var get_from_second bool = false

        get_from_first = r.MatchString(temp_cookieData)

        // $$ if not success, then deal with "smg_a"
        if !get_from_first {
            r, _ = regexp.Compile("^smg_a=({.*});")
            get_from_second = r.MatchString(temp_cookieData)
        }

   //     fmt.Println("found smg_a:", get_from_first, ", ", get_from_second)
        if get_from_first || get_from_second {
            var temp_int int
            if get_from_first {
                temp_int = 2
            } else if get_from_second {
                temp_int = 1
            }

            after_reg := r.FindStringSubmatch(temp_cookieData)[temp_int]
            //                fmt.Println("after_reg: ", after_reg)
            var smg_a_map map[string]string
            if err := json.Unmarshal([]byte(after_reg), &smg_a_map); err == nil && len(smg_a_map) > 0 {
                temp_user_agent = smg_a_map["agent"]
            }
        }
   
        

    }
     //    fmt.Println("temp_user_agent:", temp_user_agent)
        // $$ here, we should have temp_user_agent, either from user-agent or cookie
    var uaClient *uaparser.Client
    if uaData1, err := url.PathUnescape(temp_user_agent); err == nil {
        if uaData2, err := url.PathUnescape(uaData1); err == nil {
            uaClient = UAParser.Parse(uaData2)
            common.UaName = uaClient.UserAgent.Family
            common.UaDevice = uaClient.Device.Family
            common.UaOS = uaClient.Os.Family
        }
    }
//    fmt.Println("device: ", common.UaName, ", ", common.UaDevice, ", ", common.UaOS)
    
    //==========================================

    return common
}

func make_other(storm_struct map[string]interface{}, common *CdnClientCommon, data_type string) (other CdnClientOther) {

//    header_map := storm_struct["header"].(map[string]interface{})

    query_map := storm_struct["query"].(map[string]interface{})

/*
		
*/
//    var other CdnClientOther
    if data_type != "/storm/i" {

        // $$ ==============stm===================================
        /* $$
        input: 
            stm
        output:
            other.stm
        */
        // $$ turn 1561715875951 into YYYY-MM-DD hh:mm:ss
        other.stm = convert_unixtime_to_datetime(InterfaceToStr(query_map["stm"]), "mili-sec")
    //    fmt.Println("other.stm: ", return_if_map_exist(query_map, "stm"), "-->", other.stm)

    //    other.stm = return_if_map_exist(query_map, "stm")

        // $$ ===========e====================
        /* $$
        input: 
            e
        output:
            other.event
        */
        other.event = InterfaceToStr(query_map["e"])

        // $$ ===========pp_mix====================
        /* $$
        input: 
            pp_mix
        output:
            other.pp_mix
        */
        other.pp_mix = InterfaceToStr(query_map["pp_mix"])
        
    //	fmt.Println("other.pp_mix: ", other.pp_mix)

        // $$ ===========pp_max====================
        /* $$
        input: 
            pp_max
        output:
            other.pp_max
        */
        other.pp_max = InterfaceToStr(query_map["pp_max"])
        
    //	fmt.Println("other.pp_max: ", other.pp_max)

        // $$ ===========pp_miy====================
        /* $$
        input: 
            pp_miy
        output:
            other.pp_miy
        */
        other.pp_miy = InterfaceToStr(query_map["pp_miy"])
        
    //	fmt.Println("other.pp_miy: ", other.pp_miy)

        // $$ ===========pp_may====================
        /* $$
        input: 
            pp_may
        output:
            other.pp_may
        */
        other.pp_may = InterfaceToStr(query_map["pp_may"])
        
    //	fmt.Println("other.pp_may: ", other.pp_may)

        // $$ ==========url===============
        /* $$
        input: 
            url
        output:
            other.target
            other.targett
            other.articleNid
        */
        other.target, _ = url.PathUnescape(InterfaceToStr(query_map["url"]))
        other.articleNid = ""
        other.targett = "other"
        other.targett, other.articleNid = ParseTarget(other.target)

        // $$ ==========page=====================
        /* $$
        input: 
            page
        output:
            other.Articles.title
        */
        other.ArticlesTitle = InterfaceToStr(query_map["page"])

        // $$ ===========p==================
        /* $$
        input: 
            p
        output:
            other.platform
        */
        other.platform = InterfaceToStr(query_map["p"])

        // $$ ===========res=================
        /* $$
        input: 
            res
        output:
            other.screen_w
            other.screen_h
        */
        res := InterfaceToStr(query_map["res"])
        if resValues := strings.Split(strings.ToLower(res), "x"); len(resValues) == 2 {
            other.screen_w = resValues[0]
            other.screen_h = resValues[1]
        }
        
        // $$ ============uid==================
        /* $$
        input: 
            uid
        output:
            other.mid
            other.uid
        */
        uidvalue , _ := url.PathUnescape(InterfaceToStr(query_map["uid"]))
        if uids := strings.Split(strings.ToLower(uidvalue), ";"); len(uids) == 2 {
            if other.uid = uids [0] ; len(other.uid) < 1 {
                other.uid = "0"
            }
            if other.mid = uids [1] ; len(other.mid) < 1 {
                other.mid = "0"
            }     
        } else{
            other.uid = "0"
            other.mid = "0" 
        }

        // $$ ==========eid===================
        /* $$
        input: 
            eid
        output:
            other.eid
        */
        other.eid = InterfaceToStr(query_map["eid"])
        
        // $$ ============dtm================
        /* $$
        input: 
            dtm
        output:
            other.dtm
        */
    //    other.dtm = return_if_map_exist(query_map, "dtm")
        other.dtm = convert_unixtime_to_datetime(InterfaceToStr(query_map["dtm"]), "mili-sec")  
        // $$ ============vp================
        /* $$
        input: 
            vp
        output:
            other.viewport_w
            other.viewport_h
        */
        other.viewport_w = ""
        other.viewport_h = ""
        vp := InterfaceToStr(query_map["vp"])
        if vpValues := strings.Split(strings.ToLower(vp), "x"); len(vpValues) == 2 {
            other.viewport_w = vpValues[0]
            other.viewport_h = vpValues[1]
        }

        // $$ ============ds================
        /* $$
        input: 
            ds
        output:
            other.page_w
            other.page_h
        */
        other.page_w = ""
        other.page_h = ""
        ds := InterfaceToStr(query_map["ds"])
        if dsValues := strings.Split(strings.ToLower(ds), "x"); len(dsValues) == 2 {
            other.page_w = dsValues[0]
            other.page_h = dsValues[1]
        }

        // $$ ============vid================
        /* $$
        input: 
            vid
        output:
            other.vid
        */
        other.vid = InterfaceToStr(query_map["vid"])

        // $$ ============sid================
        /* $$
        input: 
            sid
        output:
            other.sid
        */
        other.sid = InterfaceToStr(query_map["sid"])
    //	fmt.Println("other.sid: ", other.sid)
        // $$ ============duid================
        /* $$
        input: 
            duid
        output:
            other.duid
        */
        other.duid = InterfaceToStr(query_map["duid"])
    //	fmt.Println("other.duid: ", other.duid)
        // $$ ============fp================
        /* $$
        input: 
            fp
        output:
            other.fp
        */
        other.fp = InterfaceToStr(query_map["fp"])

        // $$ ============fp2================
        /* $$
        input: 
            fp2
        output:
            other.fp2
        */
        other.fp2 = InterfaceToStr(query_map["fp2"])
        
        // $$ ============fp3================
        /* $$
        input: 
            fp3
        output:
            other.fp3
        */
        other.fp3 = InterfaceToStr(query_map["fp3"])

        // $$ =========refr===============
        /* $$
        input: 
            refr
        output:
            other.referer
            other.source
            other.utmSrc

        */
        // parse referer
        
        var temp_referer string = InterfaceToStr(query_map["refr"])
    //    if v, found := query_map["refr"]; found {
        //    fmt.Println("v:", v.(string))
    //        temp_referer = (v.(string))
    //    }
        // $$ because when refr is not exist, other.source should be "DIRECT", so I can't put this block
        // $$ part in the if statement
        refr, _ := url.PathUnescape(temp_referer)
        //    fmt.Println("refr:", refr)
        refererData := strings.TrimSpace(refr)
        other.referer, other.source, other.utmSrc = ParseReferer(refererData)

        //    fmt.Println("refererData: ", refererData, ", ", other.source)
        
        // $$ ============bo_flag====================
        /* $$
        input: 
            common.Cip
            common.UaDevice
            common.Cookie_smg
        output:
            other.bo_flag
        */
        // parse bo_flag
        other.bo_flag = "0"
        if common.Cip == "118.163.120.148" || common.Cip == "59.124.115.133" {
            other.bo_flag = "3"
        } else if other.bo_flag == "0" { // this is not an app
            if common.UaDevice == "Spider" {
                other.bo_flag = "1"
            } else if common.Cookie_smg == "0" {
                other.bo_flag = "5"
            }
        }
    //	fmt.Println("other.bo_flag: ", other.bo_flag)
        
        // $$ ============utm============
        /* $$
        input: 
            other.utmSrc
        output:
            other.utm
        */
        other.utm = ParseUtm(other.utmSrc)
    //	fmt.Println("other.utm: ", other.utm)

        // $$ =============scroll_st===========
        /* $$
        input: 
            other.pp_may
            other.page_h
            other.viewport_h
        output:
            other.scroll_st
        */
        scroll,_ := strconv.ParseInt("0",10,64)
        pp_may_num,_ := strconv.ParseInt(other.pp_may,10,64)
        page_h_num,_ := strconv.ParseInt(other.page_h,10,64)
        viewport_h_num,_ := strconv.ParseInt(other.viewport_h,10,64)

        if pp_may_num != 0 && (page_h_num-viewport_h_num) != 0 {
            scroll = pp_may_num*100/(page_h_num-viewport_h_num)
        }
        other.scroll_st = ""
        other.scroll_st = strconv.Itoa(int(scroll))
    //	fmt.Println("other.scroll_st: ", other.scroll_st)

        //==========================================================
    }
    
	
    return other
}
func main() {
}


func parseMap(aMap map[string]interface{}, prefix string, b *strings.Builder) {
	var sol string
	for key, val := range aMap {
		switch concreteVal := val.(type) {
		case map[string]interface{}:
			parseMap(concreteVal, prefix+key+".", b)
		case string:
			sol = key + "=" + concreteVal + " "
			b.WriteString(prefix)
			b.WriteString(sol)
			b.WriteRune(',')
		case float64:
			sol = key + "=" + strconv.FormatFloat(concreteVal, 'f', -1, 64) + " "
			b.WriteString(prefix)
			b.WriteString(sol)
            b.WriteRune(',')
        case int64:
            sol = key + "=" + strconv.FormatInt(concreteVal, 10) + " "
			b.WriteString(prefix)
			b.WriteString(sol)
            b.WriteRune(',')
        
		default:
			//What?
			panic("Unsupported")
		}
	}
}

func parsecustom(aMap map[string]interface{}) string {
	b := &strings.Builder{}
	parseMap(aMap, "", b)

	return strings.TrimSuffix(b.String(), ",")
}


func InterfaceToStr(value interface{}) string {

	switch value.(type) {
	case string:
        return value.(string)
    case []string:
        return value.([]string)[0]
	case int:
		return strconv.Itoa(value.(int))
	case int32:
		return strconv.Itoa(int(value.(int32)))
	case int64:
		return strconv.FormatInt(int64(value.(int64)), 10)
	case float64:
		return strconv.FormatFloat(value.(float64), 'f', -1, 64)
	case nil:
		return ""
	default:
		log.Println("InterFace type %T convert to string error,\n", value)
		return ""
	}
}