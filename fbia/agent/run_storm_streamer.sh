#!/bin/bash

export GODEBUG=cgocheck=0

while [ "1" == "1" ];
do
    ./sagent_storm_fbia agent_storm_fbia.conf
    if [[ $? -ne 101 ]]; then
        exit
    fi
    echo "Restart streamer after 1 minute..."
    sleep 60
done
