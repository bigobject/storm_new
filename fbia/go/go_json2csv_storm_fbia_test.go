// package name: json2csv
package main

import (
    "bytes"
    "encoding/csv"
    "log"
    "regexp"
//    "strings"
    "syscall"
    "testing"
    "time"
    "encoding/json"
    "github.com/oschwald/geoip2-golang"
    "github.com/ua-parser/uap-go/uaparser"
 //   "fmt"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
)


type CDNLogTestSuite struct {
	suite.Suite
}

func (suite *CDNLogTestSuite) SetupTest() {
    var err error

    if BOTz, err = time.LoadLocation("Asia/Taipei"); err != nil {
        log.Fatalln("Load TW time zone fail:", err)
    }

    if GeoDB, err = geoip2.Open("../common/" + GEO_DB_NAME); err != nil {
        log.Fatalln("Open geo db fail:", err)
    }

    if UAParser, err = uaparser.New("../common/" + UA_PARSER_YAML); err != nil {
        log.Fatalln("New ua parser fail:", err)
    }

    // Compiled regexp
    if len(CompiledReferRegexp) != len(ReReferSources) {
        log.Fatalln("Referer regexp structure mismatch")
    }
    for i, re := range ReReferSources {
        if CompiledReferRegexp[i], err = regexp.Compile(re.Clause); err != nil {
            log.Fatalf("Compile \"%s\" fail: %v\n", re.Clause, err)
        }
    }

    if len(CompiledUtmRegexp) != len(ReUtmSources) {
        log.Fatalln("Utm regexp structure mismatch")
    }
    for i, re := range ReUtmSources {
        if CompiledUtmRegexp[i], err = regexp.Compile(re.Clause); err != nil {
            log.Fatalf("Compile \"%s\" fail: %v\n", re.Clause, err)
        }
    }

    if CompiledFilter, err = regexp.Compile(FilterTarget); err != nil {
        log.Fatalf("Compile filter regexp fail: %v\n", err)
    }

    OutDir = "/tmp"
}

func (suite *CDNLogTestSuite) Testlog() {
//    var data = `2019-02-10	12:30:31	TPE52-C1	593	218.161.47.52	GET	d30cvjlci078zl.cloudfront.net	/i	200	-	okhttp/3.11.0	eid=2fdcfde9-39bd-4126-8219-d564fdd5c737&res=720x1344&tv=andr-0.6.2&e=ue&tna=StormNewsAppNameSpace&tz=Asia%252FTaipei&co=%257B%2522schema%2522%253A%2522iglu%253Acom.snowplowanalytics.snowplow%255C%252Fcontexts%255C%252Fjsonschema%255C%252F1-0-1%2522%252C%2522data%2522%253A%255B%257B%2522schema%2522%253A%2522iglu%253Acom.snowplowanalytics.snowplow%255C%252Fclient_session%255C%252Fjsonschema%255C%252F1-0-1%2522%252C%2522data%2522%253A%257B%2522sessionIndex%2522%253A18196%252C%2522storageMechanism%2522%253A%2522SQLITE%2522%252C%2522firstEventId%2522%253A%2522b19144db-b641-4fcf-98a6-2e49808039a3%2522%252C%2522sessionId%2522%253A%252281375435-781d-466d-b0bc-703c7ed44b76%2522%252C%2522previousSessionId%2522%253A%2522f16b5f6b-df07-423e-aac7-d2683271a764%2522%252C%2522userId%2522%253A%2522537cf88a-fd91-4715-bacd-44891da9b6c7%2522%257D%257D%252C%257B%2522schema%2522%253A%2522iglu%253Acom.snowplowanalytics.snowplow%255C%252Fmobile_context%255C%252Fjsonschema%255C%252F1-0-1%2522%252C%2522data%2522%253A%257B%2522carrier%2522%253A%2522%25E5%258F%25B0%25E7%2581%25A3%25E4%25B9%258B%25E6%2598%259F%2522%252C%2522osVersion%2522%253A%25228.1.0%2522%252C%2522osType%2522%253A%2522android%2522%252C%2522androidIdfa%2522%253A%252202cd6a69-9243-47de-bbba-07c656332204%2522%252C%2522deviceModel%2522%253A%2522Nokia%25203.1%2522%252C%2522deviceManufacturer%2522%253A%2522HMD%2520Global%2522%252C%2522networkType%2522%253A%2522wifi%2522%257D%257D%255D%257D&stm=1549801830000&p=mob&ue_pr=%257B%2522schema%2522%253A%2522iglu%253Acom.snowplowanalytics.snowplow%255C%252Funstruct_event%255C%252Fjsonschema%255C%252F1-0-0%2522%252C%2522data%2522%253A%257B%2522schema%2522%253A%2522iglu%253Amg.storm%2522%252C%2522data%2522%253A%257B%2522seqNo%2522%253A%2522%2522%252C%2522IDFA%2522%253A%252202cd6a69-9243-47de-bbba-07c656332204%2522%252C%2522percentage%2522%253A%252250%2522%252C%2522action%2522%253A%2522%2522%252C%2522duration%2520%2522%253A%25225.0%2522%252C%2522type%2522%253A%2522pageScroll%2522%252C%2522aid%2522%253A%2522925925%2522%252C%2522userId%2522%253A%25221000081%2522%252C%2522algorithm%2522%253A%2522%2522%257D%257D%257D&dtm=1549801672311&lang=%25E4%25B8%25AD%25E6%2596%2587&aid=5c6e1a	-	Hit	G-1vVNxrQKZ6OWxqKVrQViNJThpEUhHe6F6uZv42rDP7XqFnPmJ81g==	track.storm.mg	https	1240	0.110	-	TLSv1.2	ECDHE-RSA-AES128-GCM-SHA256	Hit	HTTP/2.0	-	-`

    var data = `{"storm_data":[{"time":1561715875978,"header":{"x-forwarded-for":"118.163.120.148","x-forwarded-proto":"https","x-forwarded-port":"443","host":"track.storm.mg","x-amzn-trace-id":"Root=1-5d15e4a3-dd1a2ff8e8b69bd0dff900d4","user-agent":"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 YaBrowser/19.6.0.1583 Yowser/2.5 Safari/537.36","accept":"image/webp,image/apng,image/*,*/*;q=0.8","referer":"https://www.storm.mg/authors","accept-encoding":"gzip, deflate, br","accept-language":"en,zh-TW;q=0.9,zh;q=0.8","cookie":"smg_uid=1561689505210486; uid.v=1; __auc=b5b9ca2b16b9bf2c9d46e81e39d; _ga=GA1.2.1053152044.1561689508; _gid=GA1.2.817636026.1561689508; _gaexp=GAX1.2.WGU1p3i8QZe0wSTzpzTr-A.18160.0!kVwZCBWhRcekMgEbvZNZHQ.18167.0; _sp_ses.80f4=*; _sp_id.80f4=0fff3a8a-b15a-4bc6-88c3-16cefe3ea74c.1561689508.2.1561715876.1561690068.99910fad-0fea-4448-947b-bb3a4e1a29b9"},"query":{"stm":"1561715875951","e":"pv","url":"https://www.storm.mg/authors","page":"風傳媒的作者群","tv":"js-2.9.2","tna":"cf","aid":"5c6e1a","p":"web","tz":"Asia/Shanghai","lang":"en","cs":"UTF-8","f_pdf":"1","f_qt":"0","f_realp":"0","f_wma":"0","f_dir":"0","f_fla":"0","f_java":"0","f_gears":"0","res":"1920x1080","cd":"24","cookie":"1","eid":"0f73d5c6-a53f-440d-8c95-76438b4b8ee0","dtm":"1561715875944","vp":"1119x234","ds":"1119x15235","vid":"2","sid":"99910fad-0fea-4448-947b-bb3a4e1a29b9","duid":"0fff3a8a-b15a-4bc6-88c3-16cefe3ea74c","fp":"3045323792","uid":"1561689505210486;","co":"{\"schema\":\"iglu:com.snowplowanalytics.snowplow/contexts/jsonschema/1-0-0\",\"data\":[{\"schema\":\"iglu:com.google.analytics/cookies/jsonschema/1-0-0\",\"data\":{\"_ga\":\"GA1.2.1053152044.1561689508\"}},{\"schema\":\"iglu:com.snowplowanalytics.snowplow/web_page/jsonschema/1-0-0\",\"data\":{\"id\":\"1b290e69-a303-41f3-b100-8c39bcae8736\"}}]}"},"path":"/i","version":0}]}`
//    rSrc := csv.NewReader(strings.NewReader(data))
//    rSrc.Comma = '\t'
//    cdnlog, _ := rSrc.ReadAll()
    var cdnlog map[string]interface{}
    json.Unmarshal([]byte(data), &cdnlog)

    fdCdn, fdRobot, fdCookie, fdCookieRobot := TranslateCdn(cdnlog)
//fmt.Println("in test Testlog 111")
    assert.NotEqual(suite.T(), fdCdn, -1)
    assert.Equal(suite.T(), fdRobot, -1)
    assert.Equal(suite.T(), fdCookie, -1)
    assert.Equal(suite.T(), fdCookieRobot, -1)
//fmt.Println("in test Testlog 222")
    buf := make([]byte, 4096)
    syscall.Seek(fdCdn, 0, 0)
    nRead, err := syscall.Read(fdCdn, buf)
    assert.NotEqual(suite.T(), nRead, 0)
    assert.NoError(suite.T(), err)
//fmt.Println("in test Testlog 333")
    rTgt := csv.NewReader(bytes.NewReader(buf[:nRead]))

    rows, err := rTgt.ReadAll()
    assert.Equal(suite.T(), len(rows), 1)
    assert.Equal(suite.T(), len(rows[0]), 50)

    expected := []string{"1557998531", "118.163.120.148", "", "0", "2019-05-16 17:22:11", "", "Other", "Apache-HttpClient", "Other", "prod-src-track.storm.mg", "Taipei", "TW", "Taiwan", "25.0478", "121.5318", "Asia/Taipei", "https://www.storm.mg/?srcid=7777772e73746f726d2e6d675f30653834333539353735303534623637_1557997753", "", "", "", "3", "", "other", "", "2019-05-16", "", "0", "1557997753559", "pv", "", "", "", "", "web", "1920", "1080", "55fb4037-c7c8-4dc9-8bca-9efd7f7e5d10", "1557997753554", "1067", "188", "1067", "9912", "1", "9a2ff7b5-721f-4e95-8b8e-66dcd8adb9b5", "2af671d6-b4ef-4a46-9b41-d87eeab770fe", "3750315181", "", "", "1557996432581", "0"}

//    expected := []string{"1549801831", "218.161.47.52", "", "0", "2019-02-10 20:30:31", "", "Other", "Other", "Other", "track.storm.mg", "unknown", "TW", "Taiwan", "23.5", "121", "Asia/Taipei", "", "", "G-1vVNxrQKZ6OWxqKVrQViNJThpEUhHe6F6uZv42rDP7XqFnPmJ81g==", "", "5", "DIRECT", "other", "", "2019-02-10", "", "0", "2019-02-10 20:30:30", "ue", "", "", "", "", "mob", "720", "1344", "2fdcfde9-39bd-4126-8219-d564fdd5c737", "2019-02-10 20:27:52", "", "", "", "", "", "", "", "", "", "", "0", "0"}

    assert.Equal(suite.T(), expected, rows[0])
}

func (suite *CDNLogTestSuite) TestCookieMap() {
 //   var data = `2018-04-09	06:02:48	TPE50	339	118.163.120.148	GET	d30cvjlci078zl.cloudfront.net	/storm/i	200	https://track.storm.mg/cookiemap.html?appId=5c6e1a&data=eyJhcHBJZCI6IjVjNmUxYSIsInBhZ2VUaXRsZSI6Ik5CQeOAi+eIteWjq+WuouWgtOWkp+WLnea5luS6uuOAgOaLv+S4i+Wto+W+jOizvemWgOelqC1OQkHvvZzmtJvmnYnno6/muZbkurpMb3MgQW5nZWxlcyBMYWtlcnPvvZznjLbku5bniLXlo6sgVXRhaCBKYXp6LemiqOWCs+Wqki3ph5HojILli5siLCJwYWdlVVJMIjoiaHR0cDovL3N0YWdlLnN0b3JtLm1nL2FydGljbGUvNDIyMDA4IiwiY29va2llIjoiX19nYWRzPUlEPWY2MzMyODAzNzE1ZTMwNmE6VD0xNTA1NTM5MTMxOlM9QUxOSV9NWkt3S2VZNHRfQWdfUHZfUzlPR1lKN0RqU3dqUTsgX2dhPUdBMS4yLjc0NTUwMDE1MS4xNTA1NTM5MTMwOyBfX2F1Yz0yZjVhZTUyYjE1ZTg5MjAwMzRiNmMxYWE3N2Y7IHNtZ191aWQ9MTUwNTUzOTEzMTEwMTEwMjsgX2dpZD1HQTEuMi4xNzg3MjY1MTc0LjE1MjMyNDA0Mzg7IF9zcF9zZXMuODBmND0qOyBfX2FzYz00MGZmYThkMzE2MmE4ZmRkNWExMTVmMDY5Mjg7IF90ZD1mMjY5N2NlNC0xZjIwLTRiMjYtYmRlNC0wMmM0MjgwYWMxNDA7IF9zcF9pZC44MGY0PWJkNTFjZDg1LTMyMjktNDRlYS1hOGY5LTRmNWE2M2NmODAwMC4xNTIyNzI0ODYyLjMuMTUyMzI1Mzc2My4xNTIzMjQwNzE3LmExNjM3Nzg3LThjNjItNGIzMS04OWFjLTA5NDc4NGJhYTk1OCIsInJlZmVycmVyIjoiaHR0cDovL3N0YWdlLnN0b3JtLm1nLyIsIm1lbWJlcklkIjoxMjN9	Mozilla/5.0%2520(Macintosh;%2520Intel%2520Mac%2520OS%2520X%252010_13_4)%2520AppleWebKit/537.36%2520(KHTML,%2520like%2520Gecko)%2520Chrome/65.0.3325.181%2520Safari/537.36	appId=5c6e1a&data=eyJhcHBJZCI6IjVjNmUxYSIsInBhZ2VUaXRsZSI6Ik5CQeOAi+eIteWjq+WuouWgtOWkp+WLnea5luS6uuOAgOaLv+S4i+Wto+W+jOizvemWgOelqC1OQkHvvZzmtJvmnYnno6/muZbkurpMb3MgQW5nZWxlcyBMYWtlcnPvvZznjLbku5bniLXlo6sgVXRhaCBKYXp6LemiqOWCs+Wqki3ph5HojILli5siLCJwYWdlVVJMIjoiaHR0cDovL3N0YWdlLnN0b3JtLm1nL2FydGljbGUvNDIyMDA4IiwiY29va2llIjoiX19nYWRzPUlEPWY2MzMyODAzNzE1ZTMwNmE6VD0xNTA1NTM5MTMxOlM9QUxOSV9NWkt3S2VZNHRfQWdfUHZfUzlPR1lKN0RqU3dqUTsgX2dhPUdBMS4yLjc0NTUwMDE1MS4xNTA1NTM5MTMwOyBfX2F1Yz0yZjVhZTUyYjE1ZTg5MjAwMzRiNmMxYWE3N2Y7IHNtZ191aWQ9MTUwNTUzOTEzMTEwMTEwMjsgX2dpZD1HQTEuMi4xNzg3MjY1MTc0LjE1MjMyNDA0Mzg7IF9zcF9zZXMuODBmND0qOyBfX2FzYz00MGZmYThkMzE2MmE4ZmRkNWExMTVmMDY5Mjg7IF90ZD1mMjY5N2NlNC0xZjIwLTRiMjYtYmRlNC0wMmM0MjgwYWMxNDA7IF9zcF9pZC44MGY0PWJkNTFjZDg1LTMyMjktNDRlYS1hOGY5LTRmNWE2M2NmODAwMC4xNTIyNzI0ODYyLjMuMTUyMzI1Mzc2My4xNTIzMjQwNzE3LmExNjM3Nzg3LThjNjItNGIzMS04OWFjLTA5NDc4NGJhYTk1OCIsInJlZmVycmVyIjoiaHR0cDovL3N0YWdlLnN0b3JtLm1nLyIsIm1lbWJlcklkIjoxMjN9	__gads=ID=f6332803715e306a:T=1505539131:S=ALNI_MZKwKeY4t_Ag_Pv_S9OGYJ7DjSwjQ;%2520_ga=GA1.2.745500151.1505539130;%2520__auc=2f5ae52b15e8920034b6c1aa77f;%2520smg_uid=1505539131101102;%2520_gid=GA1.2.1787265174.1523240438;%2520_sp_ses.80f4=*;%2520__asc=40ffa8d3162a8fdd5a115f06928;%2520_gat_smg_tracker=1;%2520_sp_id.80f4=bd51cd85-3229-44ea-a8f9-4f5a63cf8000.1522724862.3.1523253765.1523240717.a1637787-8c62-4b31-89ac-094784baa958;%2520_td=f2697ce4-1f20-4b26-bde4-02c4280ac140	Hit	SJcMTH6bcU9T-UkzPU2xgH0nGikzjc4XsJCAM7mWAANv7d-elwGAcw==	track.storm.mg	https	961	0.005	-	TLSv1.2	ECDHE-RSA-AES128-GCM-SHA256	Hit	HTTP/2.0	-	-`
    var data = `{"storm_data":[{"time":1561715886977,"header":{"x-forwarded-for":"118.163.120.148","x-forwarded-proto":"https","x-forwarded-port":"443","host":"track.storm.mg","x-amzn-trace-id":"Root=1-5d15e4ae-722d61881b2018e0b3cf4a18","user-agent":"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 YaBrowser/19.6.0.1583 Yowser/2.5 Safari/537.36","accept":"image/webp,image/apng,image/*,*/*;q=0.8","referer":"https://track.storm.mg/cookiemap.html?appId=5c6e1a&data=eyJhcHBJZCI6IjVjNmUxYSIsInBhZ2VUaXRsZSI6IumiqOWCs+WqkueahOS9nOiAhee+pCIsInBhZ2VVUkwiOiJodHRwczovL3d3dy5zdG9ybS5tZy9hdXRob3JzIiwiY29va2llIjoic21nX3VpZD0xNTYxNjg5NTA1MjEwNDg2OyB1aWQudj0xOyBfX2F1Yz1iNWI5Y2EyYjE2YjliZjJjOWQ0NmU4MWUzOWQ7IF9nYT1HQTEuMi4xMDUzMTUyMDQ0LjE1NjE2ODk1MDg7IF9naWQ9R0ExLjIuODE3NjM2MDI2LjE1NjE2ODk1MDg7IF9haWZwPWQxZjJlMWM3YzJkZTllYTk3MTk4ZDk1ZDYyNTEyYzdiLjE1NjE2ODk1MTE7IF9nYWV4cD1HQVgxLjIuV0dVMXAzaThRWmUwd1NUenB6VHItQS4xODE2MC4wIWtWd1pDQldoUmNla01nRWJ2Wk5aSFEuMTgxNjcuMDsgZGFibGVfdWlkPTE1MjMyODY1LjE1NjE2ODk4ODg3OTA7IF9wa19pZC4zLjgwZjQ9MDIwNWM3NTc0ODM5MWY2Zi4xNTYxNjg5ODg2LjEuMTU2MTY5MDA1Ny4xNTYxNjg5ODg2LjsgX3NwX2lkLjgwZjQ9MGZmZjNhOGEtYjE1YS00YmM2LTg4YzMtMTZjZWZlM2VhNzRjLjE1NjE2ODk1MDguMS4xNTYxNjkwMDY4LjE1NjE2ODk1MDguMmJhNWYyMmEtNzEyYy00NDhhLTgwZjMtZTM3ODY3YTQyYzMyOyBfcGtfaWQuMy41MDdlPWE5MDI5MmEwZWFkMmQzOGUuMTU2MTY4OTUwNy4yLjE1NjE3MTU4NjkuMTU2MTcxNTg2OS47IF9wa19zZXMuMy41MDdlPSoiLCJyZWZlcnJlciI6IiIsImNvb2tpZUlkIjoiMTU2MTY4OTUwNTIxMDQ4NiIsIm1lbWJlcklkIjpudWxsfQ==","accept-encoding":"gzip, deflate, br","accept-language":"en,zh-TW;q=0.9,zh;q=0.8","cookie":"smg_uid=1561689505210486; uid.v=1; __auc=b5b9ca2b16b9bf2c9d46e81e39d; _ga=GA1.2.1053152044.1561689508; _gid=GA1.2.817636026.1561689508; _gaexp=GAX1.2.WGU1p3i8QZe0wSTzpzTr-A.18160.0!kVwZCBWhRcekMgEbvZNZHQ.18167.0; _sp_ses.80f4=*; _sp_id.80f4=0fff3a8a-b15a-4bc6-88c3-16cefe3ea74c.1561689508.2.1561715876.1561690068.99910fad-0fea-4448-947b-bb3a4e1a29b9; __asc=89f36f3716b9d8521f7fb9e1ce7"},"query":{"appId":"5c6e1a","data":"eyJhcHBJZCI6IjVjNmUxYSIsInBhZ2VUaXRsZSI6IumiqOWCs WqkueahOS9nOiAhee pCIsInBhZ2VVUkwiOiJodHRwczovL3d3dy5zdG9ybS5tZy9hdXRob3JzIiwiY29va2llIjoic21nX3VpZD0xNTYxNjg5NTA1MjEwNDg2OyB1aWQudj0xOyBfX2F1Yz1iNWI5Y2EyYjE2YjliZjJjOWQ0NmU4MWUzOWQ7IF9nYT1HQTEuMi4xMDUzMTUyMDQ0LjE1NjE2ODk1MDg7IF9naWQ9R0ExLjIuODE3NjM2MDI2LjE1NjE2ODk1MDg7IF9haWZwPWQxZjJlMWM3YzJkZTllYTk3MTk4ZDk1ZDYyNTEyYzdiLjE1NjE2ODk1MTE7IF9nYWV4cD1HQVgxLjIuV0dVMXAzaThRWmUwd1NUenB6VHItQS4xODE2MC4wIWtWd1pDQldoUmNla01nRWJ2Wk5aSFEuMTgxNjcuMDsgZGFibGVfdWlkPTE1MjMyODY1LjE1NjE2ODk4ODg3OTA7IF9wa19pZC4zLjgwZjQ9MDIwNWM3NTc0ODM5MWY2Zi4xNTYxNjg5ODg2LjEuMTU2MTY5MDA1Ny4xNTYxNjg5ODg2LjsgX3NwX2lkLjgwZjQ9MGZmZjNhOGEtYjE1YS00YmM2LTg4YzMtMTZjZWZlM2VhNzRjLjE1NjE2ODk1MDguMS4xNTYxNjkwMDY4LjE1NjE2ODk1MDguMmJhNWYyMmEtNzEyYy00NDhhLTgwZjMtZTM3ODY3YTQyYzMyOyBfcGtfaWQuMy41MDdlPWE5MDI5MmEwZWFkMmQzOGUuMTU2MTY4OTUwNy4yLjE1NjE3MTU4NjkuMTU2MTcxNTg2OS47IF9wa19zZXMuMy41MDdlPSoiLCJyZWZlcnJlciI6IiIsImNvb2tpZUlkIjoiMTU2MTY4OTUwNTIxMDQ4NiIsIm1lbWJlcklkIjpudWxsfQ=="},"path":"/storm/i","version":0}]}`

//    rSrc := csv.NewReader(strings.NewReader(data))
//    rSrc.Comma = '\t'
//   cdnlog, _ := rSrc.ReadAll()

    var cdnlog map[string]interface{}
    json.Unmarshal([]byte(data), &cdnlog)

    
    fdCdn, fdRobot, fdCookie, fdCookieRobot := TranslateCdn(cdnlog)
//fmt.Println("in test TestCookieMap 111: ", fdCdn, ", ", fdRobot, ", ", fdCookie, ", ", fdCookieRobot)
    assert.Equal(suite.T(), fdCdn, -1)
    assert.Equal(suite.T(), fdRobot, -1)
    assert.NotEqual(suite.T(), fdCookie, -1)
    assert.Equal(suite.T(), fdCookieRobot, -1)
//fmt.Println("in test TestCookieMap 222")
    buf := make([]byte, 4096)
    syscall.Seek(fdCookie, 0, 0)
    nRead, err := syscall.Read(fdCookie, buf)
//  fmt.Println("in test TestCookieMap 2.5: ", fdCookie, ", ", buf, ", ", nRead, ": ", err)  
    assert.NotEqual(suite.T(), nRead, 0)
    assert.NoError(suite.T(), err)
//fmt.Println("in test TestCookieMap 333: ", nRead)
    rTgt := csv.NewReader(bytes.NewReader(buf[:nRead]))
//fmt.Println("in test TestCookieMap 444")
    rows, err := rTgt.ReadAll()
    assert.Equal(suite.T(), len(rows), 1)
    assert.Equal(suite.T(), len(rows[0]), 18)

    expected :=  []string{"1558333871", "118.163.120.148", "GA1.2.1388284722.1550043847", "1550043844973217", "2019-05-20 14:31:11", "https://track.storm.mg/cookiemap.html?appId=5c6e1a&data=eyJhcHBJZCI6IjVjNmUxYSIsInBhZ2VUaXRsZSI6IuWkj+ePjeWwiOashO+8mueCuumfk+Wci+eRnOiqquWPpeWFrOmBk+ipsS3poqjlgrPlqpIiLCJwYWdlVVJMIjoiaHR0cHM6Ly93d3cuc3Rvcm0ubWcvYXJ0aWNsZS8xMjkyOTI1IiwiY29va2llIjoic21nX3VpZD0xNTUwMDQzODQ0OTczMjE3OyB1aWQudj0xOyBfcGtfaWQuMy41MDdlPWY0YjYyOTM3NGUzMzU1ZjUuMTU1MDA0Mzg0Ni4xOC4xNTU1OTEzNjUwLjE1NTU5MTM2NDguOyBfc3BfaWQuODBmND0zOTc4Zjk2MS0xYjNhLTQ2MTYtOWU0My05ZjUzYzMxOGJlMGMuMTU1MDA0Mzg0Ni4zNC4xNTU3NzE1ODAwLjE1NTU5MTM2NjguMDRmNzZiYjgtMjI1ZS00ZGMzLWIzYzQtNTgzYjM1YzU0YjRiOyBfX2F1Yz0zOWU0ZThkODE2OGU1ZDAxNzRkYjM0NWU0ZTY7IF9nYT1HQTEuMi4xMzg4Mjg0NzIyLjE1NTAwNDM4NDc7IF9fZ2Fkcz1JRD02YzNlYjE3YjM4NTZlYTg4OlQ9MTU1MDA0Mzg0NzpTPUFMTklfTWIwLTJvYlYxcnJneXEtSHRud3FOTHFJQjM1aHc7IF9wa19pZC4zLjgwZjQ9ZjRiNjI5Mzc0ZTMzNTVmNS4xNTUwMjI1MzI4LjE0LjE1NTU5MTQ5NTUuMTU1NTkxMzY1Mi47IF9mYnA9ZmIuMS4xNTUwNDYwMzc2MjQ1LjE1NjYyMjg4NTk7IHNtZ19taWQ9MTA0Mjc3NDsgc21nX25hbWU9V2ViYmVyK0xod2FuZzsgX3RkPTk1Yjc1ODQ1LWYyOWMtNDM2ZC05YjdlLTM5NDViZGQzODE5MyIsInJlZmVycmVyIjoiIiwiY29va2llSWQiOiIxNTUwMDQzODQ0OTczMjE3IiwibWVtYmVySWQiOiIxMDQyNzc0In0=", "Other", "Firefox", "Mac OS X", "track.storm.mg", "Taipei", "TW", "Taiwan", "25.0478", "121.5318", "Asia/Taipei", "", "2019-05-20"}
//    expected := []string{"1523253768", "118.163.120.148", "GA1.2.745500151.1505539130", "1505539131101102", "2018-04-09 14:02:48", "https://track.storm.mg/cookiemap.html?appId=5c6e1a&data=eyJhcHBJZCI6IjVjNmUxYSIsInBhZ2VUaXRsZSI6Ik5CQeOAi+eIteWjq+WuouWgtOWkp+WLnea5luS6uuOAgOaLv+S4i+Wto+W+jOizvemWgOelqC1OQkHvvZzmtJvmnYnno6/muZbkurpMb3MgQW5nZWxlcyBMYWtlcnPvvZznjLbku5bniLXlo6sgVXRhaCBKYXp6LemiqOWCs+Wqki3ph5HojILli5siLCJwYWdlVVJMIjoiaHR0cDovL3N0YWdlLnN0b3JtLm1nL2FydGljbGUvNDIyMDA4IiwiY29va2llIjoiX19nYWRzPUlEPWY2MzMyODAzNzE1ZTMwNmE6VD0xNTA1NTM5MTMxOlM9QUxOSV9NWkt3S2VZNHRfQWdfUHZfUzlPR1lKN0RqU3dqUTsgX2dhPUdBMS4yLjc0NTUwMDE1MS4xNTA1NTM5MTMwOyBfX2F1Yz0yZjVhZTUyYjE1ZTg5MjAwMzRiNmMxYWE3N2Y7IHNtZ191aWQ9MTUwNTUzOTEzMTEwMTEwMjsgX2dpZD1HQTEuMi4xNzg3MjY1MTc0LjE1MjMyNDA0Mzg7IF9zcF9zZXMuODBmND0qOyBfX2FzYz00MGZmYThkMzE2MmE4ZmRkNWExMTVmMDY5Mjg7IF90ZD1mMjY5N2NlNC0xZjIwLTRiMjYtYmRlNC0wMmM0MjgwYWMxNDA7IF9zcF9pZC44MGY0PWJkNTFjZDg1LTMyMjktNDRlYS1hOGY5LTRmNWE2M2NmODAwMC4xNTIyNzI0ODYyLjMuMTUyMzI1Mzc2My4xNTIzMjQwNzE3LmExNjM3Nzg3LThjNjItNGIzMS04OWFjLTA5NDc4NGJhYTk1OCIsInJlZmVycmVyIjoiaHR0cDovL3N0YWdlLnN0b3JtLm1nLyIsIm1lbWJlcklkIjoxMjN9", "Other", "Chrome", "Mac OS X", "track.storm.mg", "unknown", "TW", "Taiwan", "23.5", "121", "Asia/Taipei", "SJcMTH6bcU9T-UkzPU2xgH0nGikzjc4XsJCAM7mWAANv7d-elwGAcw==", "2018-04-09"}
//fmt.Println("in test TestCookieMap 555")
    assert.Equal(suite.T(), expected, rows[0])
 //  fmt.Println("in test TestCookieMap 666")
}

// TestCDNLogTestSuite invokes a normal testcase for cdnlog testsuite.
func TestCDNLogTestSuite(t *testing.T) {
	suite.Run(t, new(CDNLogTestSuite))
}

func TestFBIA {
        var data =`{"storm_data":[{"query": {"res": "375x667", "vid": "495", "co": "{\"schema\":\"iglu:com.snowplowanalytics.snowplow/contexts/jsonschema/1-0-0\",\"data\":[{\"schema\":\"iglu:com.google.analytics/cookies/jsonschema/1-0-0\",\"data\":{\"_ga\":\"GA1.2.112744081.1513471606\"}},{\"schema\":\"iglu:com.snowplowanalytics.snowplow/web_page/jsonschema/1-0-0\",\"data\":{\"id\":\"4df6c38f-41c2-45cf-a3d0-8237345ef3b7\"}}]}", "p": "web", "url": "https://www.storm.mg/article/1379464", "cookie": "1", "tv": "js-2.8.2", "sid": "49ffeb4a-d145-4b53-9551-eb618301558d", "ds": "0x8", "aid": "CFe23a", "cd": "32", "e": "pv", "fp": "2782202242", "stm": "1560330058816", "tz": "Asia/Shanghai", "tna": "cf", "lang": "zh-TW", "duid": "fb9b65f5-4b08-4cdf-9d64-d2588c292797", "cs": "UTF-8", "vp": "0x0", "dtm": "1560330058805", "eid": "da932946-ca98-4e47-a7ea-e154438fdd98"}, "header": {"x-forwarded-port": "443", "accept": "image/png,image/svg+xml,image/*;q=0.8,video/*;q=0.8,*/*;q=0.5", "user-agent": "Mozilla/5.0 (iPhone; CPU iPhone OS 12_3_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/15E148 [FBAN/FBIOS;FBDV/iPhone7,1;FBMD/iPhone;FBSN/iOS;FBSV/12.3.1;FBSS/3;FBCR/&#20013-&#33775-&#38651-&#20449-;FBID/phone;FBLC/zh_TW;FBOP/5;FBIA/FBIOS]", "accept-language": "zh-tw", "host": "prod-src-track.storm.mg", "accept-encoding": "br, gzip, deflate", "referer": "https://www.storm.mg/article/1379464", "x-forwarded-for": "111.249.78.13", "x-amzn-trace-id": "Root=1-5d00bf4b-18682416ec2e7e3212b5716b", "x-forwarded-proto": "https", "cookie": "_smg_id.80f4=fb9b65f5-4b08-4cdf-9d64-d2588c292797.1519346237.495.1560330059.1560324068.49ffeb4a-d145-4b53-9551-eb618301558d; _smg_ses.80f4=*; _gat=1; __asc=15048b0616b4aeb333927a97556; __auc=cb2f168f16061effb81b5175662; _ga=GA1.2.112744081.1513471606; _gid=GA1.2.1454286243.1560117704; _sp_id.80f4=01b3f507-f5bb-4294-a1f1-c81e6da864a1.1524325495.145.1559956681.1559654143.f9a4a300-a0f3-48b9-a131-12d3c24ea9bd; _td=6137abff-2f1d-4cc8-9e71-95e6d80e6847; _fbp=fb.1.1556092360406.642828111; smg_uid=1513684560638784; uid.v=1; __gads=ID=fa7d3862a8f281b7:T=1513684872:S=ALNI_MY9WeqQpqb8JStXIKowAP10nrtQCQ"}, "version": 0, "time": 1560330059224}]}`
}