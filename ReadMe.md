# 風傳媒 storm 的 client, fbia, app ver 1.0



## Feature

1. input 從以前的 csv 轉成 json.
2. 目前仍是用 streamer 1.0
2. 加了db_time 的三個真正 parsing 的 timestamp.
3. 新增 error log. 當某狀況發生需要把整筆資料丟掉時, 傳 error 給 C++ 並且寫一個 txt 格式的 error log, 一天最多一個txt file.


## 待修正

1. 將來改成 streamer 3.0
2. 裡面有許多沒用的code和註解, 需要整理

