// package name: json2csv
package main

import (
    "C"
    "bytes"
    "encoding/csv"
    "encoding/json"
    "log"
    "net"
    "net/url"
    "strconv"
    "strings"
    "sync"
    "syscall"
    "time"
    "fmt"
    "regexp"
    "github.com/oschwald/geoip2-golang"
    "github.com/ua-parser/uap-go/uaparser"
    "reflect"
)

const (
    BO_TmFmt = "2006-01-02 15:04:05"
    BO_TMPFILE = 0x410000

    GEO_DB_NAME = "GeoLite2-City.mmdb"
    UA_PARSER_YAML = "regexes.yaml"
)



type RegexpClause struct {
    Source      string
    Clause      string
}



var (
    once        sync.Once
    OutDir      string
    BOTz        *time.Location

    GeoDB       *geoip2.Reader
    UAParser    *uaparser.Parser
)

// These are output for C to avoid GC
var (
    out_err_ptr     []string
    out_tbl_ptr     [][]string
    out_fd_ptr      [][]int
)


type UePr struct {
    Data    UePrData `json:"data"`
}

type UePrData struct { 
    Data    UePrInfo `json:"data"`
}

type UePrInfo struct {
    Seqno      string    `json:"seqNo"`
    Aid        string    `json:"aid"`
    Nid        string    `json:"nid"`
    Action     string    `json:"action"`
    Type       string    `json:"type"`
    Algorithm  string    `json:"algorithm"`
    Userid     string    `json:"userId"`
    DevId      string    `json:"IDFA"`
    Duration   string    `json:"duration "`
    Scroll     string    `json:"percentage"`
    Scontent   string    `json:"content"`
}


type Algo struct {
    Channel_id   string      `json:"channel_id"`
    Query_from   string      `json:"query_from"`
    Query_to     string      `json:"query_to"`
    Score        float64     `json:"score"`
    Bias         Biasinfo    `json:"bias"`
    Assigned     Assigninfo  `json:"assigned"`
    Ur           Urinfo      `json:"ur"`
    Lda          Ldainfo     `json:"lda"`
    Latest       Latestinfo  `json:"latest"`
	Name		string		 `json:"name"`
	Query_time	string		 `json:"query_time"`
}

type Urinfo struct {
    Score    float64  `json:"score"`
    Weight   float64  `json:"weight"`
    Scale    float64  `json:"scale"`
}

type Ldainfo struct {
    Score    float64  `json:"score"`
    Weight   float64  `json:"weight"`
}

type Latestinfo struct {
    Score    float64  `json:"score"`
    Weight   float64  `json:"weight"`
}

type Biasinfo struct {
    Schema     string      `json:"schema"`
    Rule       []BiasRule    `json:"rule"`  
}

type BiasRule struct {
    Name     string  `json:"name"`
    Value    []float64  `json:"value"`
    Values    []float64  `json:"values"`
    Bias     float64     `json:"bias"`  
}

type Assigninfo struct {
    Score     float64  `json:"score"`
    Weight    float64  `json:"weight"`  
}

type AppCo struct {
    Data    []CoData `json:"data"`
}

type CoData struct {
    Data    CoInfo `json:"data"`
}

type CoInfo struct {
    NetType         string `json:"networkType"`
    OsType          string `json:"osType"`
    OsVer           string `json:"osVersion"`
    AppleId         string `json:"appleIdfv"`
    AndroidId       string `json:"androidIdfa"`
    Carrier         string `json:"carrier"`
    Manufacturer    string `json:"deviceManufacturer"`
    Model           string `json:"deviceModel"`
    PrevSessionId   string `json:"previousSessionId"`
    SessionId       string `json:"sessionId"`
    SessionIndex    int    `json:"sessionIndex"`
    UserId          string `json:"userId"`
    FirstEventId    string `json:"firstEventId"`

    SeqNo           string `json:"seqNo"`
    Action          string `json:"action"`
    Type            string `json:"type"`
    Aid             string `json:"aid"`
    Alg             string `json:"algorithm"`
}

type CdnFBAICommon struct {
    InputTs         string
    InputDateTime   string
    InputDate       string
    Cip             string
    Cookie_ga       string
    Cookie_smg      string
    UaDevice        string
    UaName          string
    UaOS            string
    HostHeader      string
    City            string
    CountryCode     string
    Country         string
    Latitude        string
    Longitude       string
    Tz              string
    ReqId           string

    Db_time         string
    
    Db_lagtime      string
}

type CdnFBAIOther struct {
    uepr_type            string     // 'app_eventname' STRING(63),
    osType               string // 'app_ostype' STRING(50),
    networkType          string // 'app_networktype' STRING(50),
    osVer                string // 'app_osversion' CHAR(20),
    devId                string // 'app_deviceid' VARSTRING(100),
    carrier              string // 'carrier.id' STRING(63),
    manufacturer         string // 'app_devicemanufacturer' STRING(63),
    model                string // 'app_devicemodel' STRING(63),
    prevSessionId        string // 'app_previoussessionId' VARSTRING(200),
    firstEventId         string // 'app_firsteventId' VARSTRING(200),
    sessionId            string // 'app_sessionid' VARSTRING(200),
    userId               string // 'app_userid' STRING(200),
    sessionIndex         string // 'app_sessionindex' INT64,
    stm                  string // 'app_senttime' DATETIME32,
    dtm                  string // 'app_devtime' DATETIME32,
    lang                 string // 'lang.id' CHAR(50),
    screen_w             string // 'screen_w' INT32,
    screen_h             string // 'screen_h' INT32,
    uepr_seqno           string // 'app_seqno' INT16,
    algorithmRaw         string // 'app_algorithm' VARSTRING(60000),
    channel_id           string // 'channel_id' INT32,
    query_from_fmt       string // 'query_from' DATETIME32,
    query_to_fmt         string // 'query_to' DATETIME32,
    score                string // 'score' FLOAT,
    peo                  string // 'peo' INT8,
    aid                  string // 'aid' STRING(20),
    bias_schema          string // 'bias_schema' STRING(50),
    bias_value           string // 'bias_value' INT32,
    bias_score           string // 'bias_score' FLOAT,
    bias_weight          string // 'bias_weight' FLOAT,
    lda_weight           string // 'lda_w' FLOAT,
    lda_score            string // 'lda_s' FLOAT,
    ur_weight            string // 'ur_w' FLOAT,
    ur_score_str         string // 'ur_s' FLOAT,
    ur_scale_str         string // 'ur_scale' FLOAT,
    ur_org               string // 'ur_original' FLOAT,
    latest_weight        string // 'latest_w' FLOAT,
    latest_score         string // 'latest_s' FLOAT,
    uepr_action          string // 'app_action' STRING(63),
    platform             string // 'app_platform' STRING(63),
    eid                  string // 'eid' VARSTRING(100),
    fp                   string // 'app_fingerprint' STRING(63),
    appTz                string // 'app_tz' STRING(63),
    articleNid           string // 'articles.nid' INT64,
    memberid             string // 'member.mid' INT32
    duration             string // 'duration' float
    scroll               string // 'scroll' INT64
    scontent             string // 'scroll' INT64
    name                 string 

    Db_app_sent_lagtime string // Db_app_sent_lagtime INT64
    Db_app_dev_lagtime  string // Db_app_dev_lagtime INT64
}



func ParseAlgorithm(Algorithm string)(channel_id,score,query_from_fmt,query_to_fmt,bias_schema,peo,bias_score,bias_weight,bias_value string,ur_score float64,ur_weight string,ur_scale float64 ,lda_score,lda_weight,latest_score,latest_weight string,name string){
    algo := Algo{}
    query_from := ""        
    query_to := ""          
    if err := json.Unmarshal([]byte(Algorithm), &algo); err != nil {
        log.Printf("Parse algo (%s) fail: %v\n", Algorithm, err)
    }
    channel_id = algo.Channel_id
    score = strconv.FormatFloat(algo.Score, 'f', 4, 64)
	if name = algo.Name ;name!=""{
		query_from = algo.Query_time
		query_to = algo.Query_time
	}else {
        name = "engine"
		query_from = algo.Query_from
		query_to = algo.Query_to
	}
    
    if t, err := time.Parse(time.RFC3339, query_from); err == nil {
                tCst := t.In(BOTz)
                query_from_fmt = tCst.Format(BO_TmFmt)   
    }
    
    if t, err := time.Parse(time.RFC3339, query_to); err == nil {
                tCst := t.In(BOTz)
                query_to_fmt = tCst.Format(BO_TmFmt)
    }
	
    if bias_schema = algo.Bias.Schema;bias_schema != "" {
        peo = "1"
        if bias_schema == "assigned" {
            bias_score = strconv.FormatFloat(algo.Assigned.Score, 'f', 4, 64)
            bias_weight = strconv.FormatFloat(algo.Assigned.Weight, 'f', 4, 64)
        } else if bias_schema == "condition" {
            bias_score = strconv.FormatFloat(algo.Bias.Rule[0].Bias, 'f', 4, 64)             
            if len(algo.Bias.Rule[0].Value) > 0 {
                bias_value = strconv.Itoa(int(algo.Bias.Rule[0].Value[0]))
            } else { 
                bias_value = strconv.Itoa(int(algo.Bias.Rule[0].Values[0]))
            } 
        }
    }

    
	
	
    ur_score = algo.Ur.Score
    ur_weight = strconv.FormatFloat(algo.Ur.Weight, 'f', 4, 64) 
    ur_scale = algo.Ur.Scale
    lda_score = strconv.FormatFloat(algo.Lda.Score, 'f', 4, 64)
    lda_weight = strconv.FormatFloat(algo.Lda.Weight, 'f', 4, 64) 
    latest_score = strconv.FormatFloat(algo.Latest.Score, 'f', 4, 64)
    latest_weight = strconv.FormatFloat(algo.Latest.Weight, 'f', 4, 64)

    return channel_id,score,query_from_fmt,query_to_fmt,bias_schema,peo,bias_score,bias_weight,bias_value,ur_score,ur_weight,ur_scale,lda_score,lda_weight,latest_score,latest_weight,name
}


func WriteToFile(records [][]string) (fd int, err error) {

    if fd, err = syscall.Open(OutDir, syscall.O_RDWR|BO_TMPFILE|syscall.O_EXCL|syscall.O_CLOEXEC, 0666); err != nil {
        return fd, err
    }

    wBuffer := new(bytes.Buffer)
    wBuffer.Grow(8196)
    w := csv.NewWriter(wBuffer)
    w.WriteAll(records)
    if err := w.Error(); err != nil {
        syscall.Close(fd)
        fd = -1
        return fd, err
    }

    syscall.Write(fd, wBuffer.Bytes())
    return fd, nil
}


// AWS CDNLOG format
//  0: date,2018-09-20
//  1: time,02:30:52
//  2: x-edge-location,TPE51-C1
//  3: sc-bytes,522
//  4: c-ip,114.136.231.71
//  5: cs-method,GET
//  6: cs(Host),d30cvjlci078zl.cloudfront.net
//  7: cs-uri-stem,/i
//  8: sc-status,200
//  9: cs(Referer),https://www.storm.mg/article/504303
// 10: cs(User-Agent),Mozilla/5.0%2520(Linux;%2520Android%25205.0.2;%2520HTC_M9ew%2520Build/LRX22G;%2520wv)%2520AppleWebKit/537.36%2520(KHTML,%2520like%2520Gecko)%2520Version/4.0%2520Chrome/68.0.3440.91%2520Mobile%2520Safari/537.36%2520%5BFBIA/FB4A;FBAV/166.0.0.63.95;%5D
// 11: cs-uri-query,stm=1537410649854&e=pv&url=https%253A%252F%252Fwww.storm.mg%252Farticle%252F504303&tv=js-2.8.2&tna=cf&aid=CFe23a&p=web&tz=Asia%252FShanghai&lang=zh-TW&cs=UTF-8&res=360x640&cd=24&cookie=1&eid=3db75f1f-0a19-47f8-bef1-fd8549ca066b&dtm=1537410649840&vp=1x1&ds=1x8&vid=49&sid=9d694f6a-fa52-4d3d-88f4-23e26d45d256&duid=a5712449-9637-4947-ab0f-46771334968d&fp=3360602253&co=%257B%2522schema%2522%253A%2522iglu%253Acom.snowplowanalytics.snowplow%252Fcontexts%252Fjsonschema%252F1-0-0%2522%252C%2522data%2522%253A%255B%257B%2522schema%2522%253A%2522iglu%253Acom.google.analytics%252Fcookies%252Fjsonschema%252F1-0-0%2522%252C%2522data%2522%253A%257B%2522_ga%2522%253A%2522GA1.2.274733820.1528723644%2522%257D%257D%252C%257B%2522schema%2522%253A%2522iglu%253Acom.snowplowanalytics.snowplow%252Fweb_page%252Fjsonschema%252F1-0-0%2522%252C%2522data%2522%253A%257B%2522id%2522%253A%25224821be49-ab03-45e7-9267-d98d054e6c20%2522%257D%257D%255D%257D
// 12: cs(Cookie),__auc=211f0b96163ef079ce23aa4a188;%2520_ga=GA1.2.274733820.1528723644;%2520smg_uid=1528723646128350;%2520uid.v=1;%2520_gid=GA1.2.200108240.1537339202;%2520__asc=66ba37e0165f4d0c5f6da99ca43;%2520_smg_ses.80f4=*;%2520_gat=1;%2520_smg_id.80f4=a5712449-9637-4947-ab0f-46771334968d.1528723644.49.1537410650.1537339204.9d694f6a-fa52-4d3d-88f4-23e26d45d256
// 13: x-edge-result-type,Hit
// 14: x-edge-request-id,k7wVQpi0hjJBVqYKqXAlVLE_2M8vtdkLebSrZsAPj_NabfA0Gy0KLw==
// 15: x-host-header,track.storm.mg
// 16: cs-protocol,https
// 17: cs-bytes,1101
// 18: time-taken,0.064
// 19: x-forwarded-for,-
// 20: ssl-protocol,TLSv1.2
// 21: ssl-cipher,ECDHE-RSA-AES128-GCM-SHA256
// 22: x-edge-response-resultHit
// 23: cs-protocol-version,HTTP/2.0
// 24: fle-status,-
// 25: fle-encrypted-fields,-

// func TranslateCdn(cdnlog [][]string) (fdCdn int) {
func TranslateCdn(data_map map[string]interface{}, fatal_error *string) (fdCdn int) {
    fdCdn = -1

    logRecords := [][]string{}

    storm_struct := data_map

    header_map := storm_struct["header"].(map[string]interface{})
    query_map := storm_struct["query"].(map[string]interface{})

    data_type := ""

    // $$ app --> /i
    
    if temp_path, found := storm_struct["path"].(string); found {
        
        switch temp_path {
        case "/i":
            if InterfaceToStr(query_map["aid"]) != "StormNewsApp" && InterfaceToStr(query_map["aid"]) != "StormNewsApp2" {
                temp_msg := "Invalid aid: " + InterfaceToStr(query_map["aid"]) 
                *fatal_error = temp_msg
                return -1// continue $$ 2019/07/05 Frank: delete "storm_data" structure
            } else {
                data_type = "/i"
            }
        default:
            return -1// continue $$ 2019/07/05 Frank: delete "storm_data" structure // $$ not valid path, skip it and procced to next records // 
        }
    } else {
        
        temp_msg := "path missing"
        *fatal_error = temp_msg
        fmt.Println(temp_msg) 
        return  -1
    }
    
    temp_host := InterfaceToStr(header_map["host"])
    if temp_host != "prod-src-track.storm.mg" && temp_host != "track.storm.mg" {
        temp_msg := "Invalid host: " + temp_host
        *fatal_error = temp_msg
        log.Println(temp_msg)
        return -1   
    }
//===================================================
    var common CdnFBAICommon
    var other CdnFBAIOther

    common = make_common(storm_struct)
    other = make_other(storm_struct)

    if data_type == "/i" {
        record := TranslateLog(&common, &other)

        logRecords = append(logRecords, [][]string{record}...)
    }
//================================================
    

    var err error
    if len(logRecords) > 0 {
        if fdCdn, err = WriteToFile(logRecords); err != nil {
            log.Println("write cdnlog_app data fail:", err)
        }
    }
    return fdCdn
}

//export SetCounter
func SetCounter(_ string, _ int) {
}

//export SetConfig
func SetConfig(_ string) {
}

//export JsonToCsv
func JsonToCsv(inFd []int, outPath string) (out_tbl [][]string, out_fd [][]int, err_str []string) {
    once.Do(func() {
        var err error

        if BOTz, err = time.LoadLocation("Asia/Taipei"); err != nil {
            log.Fatalln("Load TW time zone fail:", err)
        }

        if GeoDB, err = geoip2.Open("../common/" + GEO_DB_NAME); err != nil {
            log.Fatalln("Open geo db fail:", err)
        }

        UAParser, err = uaparser.New("../common/" + UA_PARSER_YAML)


        OutDir = outPath
    })

    var wg sync.WaitGroup
    err_str = make([]string, len(inFd))
    out_tbl = make([][]string, len(inFd))
    out_fd  = make([][]int, len(inFd))

    for idx, jFd := range inFd {
        wg.Add(1)
        go func(idx, jFd int) {
            defer func() {
                wg.Done()
            }()

            //========================================
            var inStat syscall.Stat_t
            if err := syscall.Fstat(jFd, &inStat); err != nil {
                fmt.Printf("Get input file state fail: %v\n", err)
                err_str[idx] = err.Error()
                return
            }

            jsonData, err := syscall.Mmap(jFd, 0, int(inStat.Size), syscall.PROT_READ, syscall.MAP_SHARED)
            if err != nil {
                fmt.Printf("Mmap input file fail: %v\n", err)
                err_str[idx] = err.Error()
                return
            }
            defer syscall.Munmap(jsonData)
            //===============================================
            /*
            r := csv.NewReader(bytes.NewReader(jsonData))
            r.Comma = '\t'
            r.Comment = '#'
            records, err := r.ReadAll()
            if err != nil {
                log.Println("Read csv fail:", err)
                err_str[idx] = err.Error()
                return
            }

            out_tbl[idx] = []string{}
            out_fd[idx] = []int{}

            fdCdn := TranslateCdn(records)
            */
            jsonData = bytes.Replace(jsonData, []byte("NaN"), []byte("0"), -1)

            var data_map map[string]interface{}

            fdCdn := -1
            var fatal_error string

            if err = json.Unmarshal([]byte(jsonData), &data_map); err == nil && len(data_map) > 0 {
            //    public_cookie :=  parsecustom(data_map)
                
                fdCdn = TranslateCdn(data_map, &fatal_error)
                err_str[idx] = fatal_error
            //    fmt.Println("TranslateCdn, idx: ", idx, ": ", fdCdn, ", ", fdRobot, ", ", fdCookie, ", ", fdCookieRobot )
            } else {
                fatal_error = "Parse error: " + err.Error()
                fmt.Println(fatal_error)
                err_str[idx] = fatal_error
                
            }

            out_tbl[idx] = []string{}
            out_fd[idx] = []int{}

            if fdCdn > 0 {
                out_tbl[idx] = append(out_tbl[idx], []string{"cdnlog_app"}...)
                out_fd[idx] = append(out_fd[idx], []int{fdCdn}...)
            }
        }(idx, jFd)
    }
    wg.Wait()

    // keep last output reference to avoid GC after return to C program
    out_err_ptr = err_str
    out_tbl_ptr = out_tbl
    out_fd_ptr  = out_fd

    return out_tbl, out_fd, err_str
}

func main() {
}


func TranslateLog(common *CdnFBAICommon, other *CdnFBAIOther) ([] string){

    log := make([]string, 71)       // <<--- public_cookie 記得改回來67
    
    log[0]   = common.InputTs              // 'ts' TIMESTAMP,
    log[1]   = other.uepr_type            // 'app_eventname' STRING(63),
    log[2]   = other.osType               // 'app_ostype' STRING(50),
    log[3]   = other.networkType          // 'app_networktype' STRING(50),
    log[4]   = other.osVer                // 'app_osversion' CHAR(20),
    log[5]   = other.devId                // 'app_deviceid' VARSTRING(100),
    log[6]   = other.carrier              // 'carrier.id' STRING(63),
    log[7]   = other.manufacturer         // 'app_devicemanufacturer' STRING(63),
    log[8]   = other.model                // 'app_devicemodel' STRING(63),
    log[9]   = other.prevSessionId        // 'app_previoussessionId' VARSTRING(200),
    log[10]  = other.firstEventId         // 'app_firsteventId' VARSTRING(200),
    log[11]  = other.sessionId            // 'app_sessionid' VARSTRING(200),
    log[12]  = other.userId               // 'app_userid' STRING(200),
    log[13]  = other.sessionIndex         // 'app_sessionindex' INT64,
    log[14]  = other.stm                  // 'app_senttime' DATETIME32,
    log[15]  = other.dtm                  // 'app_devtime' DATETIME32,
    log[16]  = other.lang                 // 'lang.id' CHAR(50),
    log[17]  = other.screen_w             // 'screen_w' INT32,
    log[18]  = other.screen_h             // 'screen_h' INT32,
    log[19]  = common.ReqId     //row[14]        // 'requestid' VARSTRING(300),
    log[20]  = other.uepr_seqno           // 'app_seqno' INT16,
    log[21]  = other.algorithmRaw         // 'app_algorithm' VARSTRING(60000),
    log[22]  = other.channel_id           // 'channel_id' INT32,
    log[23]  = other.query_from_fmt       // 'query_from' DATETIME32,
    log[24]  = other.query_to_fmt         // 'query_to' DATETIME32,
    log[25]  = other.score                // 'score' FLOAT,
    log[26]  = other.peo                  // 'peo' INT8,
    log[27]  = other.aid                  // 'aid' STRING(20),
    log[28]  = other.bias_schema          // 'bias_schema' STRING(50),
    log[29]  = other.bias_value           // 'bias_value' INT32,
    log[30]  = other.bias_score           // 'bias_score' FLOAT,
    log[31]  = other.bias_weight          // 'bias_weight' FLOAT,
    log[32]  = other.lda_weight           // 'lda_w' FLOAT,
    log[33]  = other.lda_score            // 'lda_s' FLOAT,
    log[34]  = other.ur_weight            // 'ur_w' FLOAT,
    log[35]  = other.ur_score_str         // 'ur_s' FLOAT,
    log[36]  = other.ur_scale_str         // 'ur_scale' FLOAT,
    log[37]  = other.ur_org               // 'ur_original' FLOAT,
    log[38]  = other.latest_weight        // 'latest_w' FLOAT,
    log[39]  = other.latest_score         // 'latest_s' FLOAT,
    log[40]  = other.uepr_action          // 'app_action' STRING(63),
    log[41]  = other.platform             // 'app_platform' STRING(63),
    log[42]  = other.eid                  // 'eid' VARSTRING(100),
    log[43]  = other.fp                   // 'app_fingerprint' STRING(63),
    log[44]  = other.appTz                // 'app_tz' STRING(63),
    log[45]  = common.Cip                  // 'cip' IPv6,
    log[46]  = common.Cookie_ga            // 'cookiega' VARSTRING(30),
    log[47]  = common.Cookie_smg           // 'cookieuid' INT64,
    log[48]  = common.InputDateTime        // 'inputdate' DATETIME32,
    log[49]  = common.UaDevice             // 'agentdevice' STRING(50),
    log[50]  = common.UaName               // 'agentname' STRING(30),
    log[51]  = common.UaOS                 // 'agentosname' STRING(15),
    log[52]  = common.HostHeader    //  row[15]       // 'xhostheader' CHAR(30),
    log[53]  = common.City                 // 'ipcityname' STRING(64),
    log[54]  = common.CountryCode          // 'ipcountrycode2' STRING(3),
    log[55]  = common.Country              // 'ipcountryname' STRING(64),
    log[56]  = common.Latitude             // 'iplatitude' FLOAT,
    log[57]  = common.Longitude            // 'iplongitude' FLOAT,
    log[58]  = common.Tz                   // 'iptimezone' CHAR(30),
    log[59]  = other.articleNid           // 'articles.nid' INT64,
    log[60]  = ""                   // 'tag' STRING(63),
    log[61]  = common.InputDate            // 'date' CHAR(11),
    log[62]  = other.memberid             // 'member.mid' INT32
    log[63]  = other.duration             // 'duration' float
    log[64]  = other.scroll               // 'scroll' INT64

    log[65]  = other.scontent             
    log[66]  = other.name 

    log[67] = common.Db_time
    log[68] = common.Db_lagtime             // Db_lagtime INT64
    log[69] = other.Db_app_sent_lagtime     // Db_app_sent_lagtime INT64
    log[70] = other.Db_app_dev_lagtime      // Db_app_dev_lagtime INT64

//    log[71] = public_cookie
    
    return log

}

func make_common(storm_struct map[string]interface{}) (common CdnFBAICommon) {

    var err error
    var ok bool

    header_map := storm_struct["header"].(map[string]interface{})

    query_map := storm_struct["query"].(map[string]interface{})
    /*
    $$
        I don't know how to parse it
        
        "cookie" is not found in the sample data
        "fp" is not found in the sample data
        cs-uri-query
    */
/*
    if len(row) < 26 {
        log.Println("Invalid row data:", row)
        continue
    }
    if row[8] != "200" { // http response status
        continue
    }

    if row[7] != "/i" {
        continue
    }
*/
/*
    // row[11]: cs-uri-query
    uriValues, err := url.ParseQuery(row[11])
    if err != nil {
        log.Printf("Fail to parse uri (%s): %v\n", row[11], err)
        return -1
    }
*/
 
    // $$ ========== time===========
    /* $$
    input: 
        time
    output:
        common.InputTs
        common.InputDateTime
        common.InputDate

        common.Db_time
        common.Db_lagtime

    */
    UnixTime_time := convert_unixtime_to_time(storm_struct["time"], "mili-sec")	

    current_unix_10 := strconv.FormatInt(time.Now().UTC().Unix(), 10) // $$ 10 digits(sec), ex: 1557998531388

    common.Db_time = convert_unixtime_to_datetime(current_unix_10, "sec") // $$ YYYY-MM-DD hh:mm:ss


    // $$ common.InputTs want 10 digits, but storm_struct["time"] is 13 digits
    common.InputTs = strconv.FormatInt(UnixTime_time.Unix(), 10) // 1557998531388

    var temp_InputTs int64
    temp_InputTs, err = strconv.ParseInt(common.InputTs, 10, 64)
    
    common.Db_lagtime = strconv.FormatInt(get_currenttime_int64() - temp_InputTs, 10) // $$ get lag time in seconds


    
    // $$ change to CST

//    tCst := UnixTime_time.In(BOTz)
//    common.InputDateTime = tCst.Format(BO_TmFmt) // 2019-05-16 17:22:11

    common.InputDateTime = convert_unixtime_to_datetime(storm_struct["time"], "mili-sec")	
    common.InputDate = strings.Split(common.InputDateTime," ")[0] // 2019-05-16

    // $$ ==========cookie==========================
    /* $$
    input: 
        cookie
    output:
        common.Cookie_ga
        common.Cookie_smg   
    */
    // parse Cookies
    
    var temp_cookie string
    if temp_cookie = InterfaceToStr(header_map["cookie"]); temp_cookie == "" {
        temp_cookie = InterfaceToStr(query_map["cookie"])
    }
    
    var temp_cookieData string
    cookieKv := make(map[string]string)
    if cookieDataTmp, temp_err := url.QueryUnescape(temp_cookie); temp_err == nil {
        if cookieData, temp_err := url.QueryUnescape(cookieDataTmp); temp_err == nil {
            //==============================
            temp_cookieData = cookieData // $$ make a copy for later use (user-agent)
        //    fmt.Println("cookieData: ", cookieData)
            
            //=================================
            cookies := strings.Split(cookieData, "; ")
        
            for _, aCookie := range cookies {
                kv := strings.SplitN(aCookie, "=", 2)
                if len(kv) == 2 {
                    
                    cookieKv[kv[0]] = kv[1]
                }
            }
        }
    }

    // get cookies we are interested  
    common.Cookie_ga, ok = cookieKv["_ga"]
    if !ok {
        common.Cookie_ga = ""
    }
    common.Cookie_smg, ok = cookieKv["smg_uid"]
    if !ok {
        common.Cookie_smg = "0"
    } else {
        if _, err = strconv.ParseInt(common.Cookie_smg, 10, 64); err != nil { // it's an array.
            uids := []int64{}
            if err = json.Unmarshal([]byte(common.Cookie_smg), &uids); err == nil && len(uids) > 0 {
                common.Cookie_smg = strconv.FormatInt(uids[0], 10)
            }
        }
    }

    // $$ ========x-forwarded-for==============
    /* $$
    input: 
        x-forwarded-for
    output:
        common.Cip
        common.Latitude
        common.Longitude
        common.Tz
        common.City
        common.Country
        common.CountryCode
    */
    temp_ip_array := strings.Split(InterfaceToStr(header_map["x-forwarded-for"]), ",")
    common.Cip = strings.TrimSpace(temp_ip_array[0])

    city := "unknown"
    country := "unknown"
    countryCode := "unknown"
    ip := net.ParseIP(common.Cip)
    if geoCity, temp_err := GeoDB.City(ip); temp_err == nil {
        countryCode = geoCity.Country.IsoCode
        if city, ok = geoCity.City.Names["en"]; !ok {
            city = "unknown"
        }
        if country, ok = geoCity.Country.Names["en"]; !ok {
            country = "unknown"
            countryCode = "unknown"
        }


        common.Latitude = strconv.FormatFloat(geoCity.Location.Latitude, 'f', -1, 32)
        common.Longitude = strconv.FormatFloat(geoCity.Location.Longitude, 'f', -1, 32)
        common.Tz = geoCity.Location.TimeZone
        // continentCode = geoCity.Continent.Code
    }

    common.City = city
    common.Country = country
    common.CountryCode = countryCode
    

    
    // $$ ==========user-agent==========================
    /* $$
    input: 
        user-agent or cookie --> smg_a --> agent
    output:
        common.UaName
        common.UaDevice
        common.UaOS
    */
    var temp_user_agent string
    
    temp_user_agent = strings.TrimSpace(InterfaceToStr(header_map["user-agent"]))
    if temp_user_agent == "" || temp_user_agent == "Amazon CloudFront" {
    //    fmt.Println("temp_cookieData: ", temp_cookieData)
        // $$ $$ deal with "XXX; smg_a" or " smg_a"
        r, _ := regexp.Compile("([^ \f\n\r\t\v]+;)*[ \f\n\r\t\v]+smg_a=({.*});")

        var get_from_first bool = false
        var get_from_second bool = false

        get_from_first = r.MatchString(temp_cookieData)

        // $$ if not success, then deal with "smg_a"
        if !get_from_first {
            r, _ = regexp.Compile("^smg_a=({.*});")
            get_from_second = r.MatchString(temp_cookieData)
        }

   //     fmt.Println("found smg_a:", get_from_first, ", ", get_from_second)
        if get_from_first || get_from_second {
            var temp_int int
            if get_from_first {
                temp_int = 2
            } else if get_from_second {
                temp_int = 1
            }

            after_reg := r.FindStringSubmatch(temp_cookieData)[temp_int]
            //                fmt.Println("after_reg: ", after_reg)
            var smg_a_map map[string]string
            if err := json.Unmarshal([]byte(after_reg), &smg_a_map); err == nil && len(smg_a_map) > 0 {
                temp_user_agent = smg_a_map["agent"]
            }
        }
    }

     //    fmt.Println("temp_user_agent:", temp_user_agent)
        // $$ here, we should have temp_user_agent, either from user-agent or cookie
        
    var uaClient *uaparser.Client
    if uaData1, err := url.PathUnescape(temp_user_agent); err == nil {
        if uaData2, err := url.PathUnescape(uaData1); err == nil {
            uaClient = UAParser.Parse(uaData2)
            common.UaName = uaClient.UserAgent.Family
            common.UaDevice = uaClient.Device.Family
            common.UaOS = uaClient.Os.Family
        }
    }

    // $$ ========x-amzn-trace-id==============
    /* $$
    input: 
        x-amzn-trace-id
    output:
        common.ReqId
    */
    common.ReqId = InterfaceToStr(header_map["x-amzn-trace-id"])

    // $$ ========host==============
    /* $$
        prod-src-track.storm.mg --> track.storm.mg
    input: 
        host
    output:
        common.HostHeader
    */
    common.HostHeader = strings.TrimPrefix(InterfaceToStr(header_map["host"]), "prod-src-")

    return common
}

func make_other(storm_struct map[string]interface{}) (other CdnFBAIOther) { 

    var err error
    query_map := storm_struct["query"].(map[string]interface{})
    // $$ ==========aid===========
    /* $$
    input: 
        aid
    output:
        other.aid
    */
    other.aid = InterfaceToStr(query_map["aid"])

    // $$ ========tz==============
    /* $$
    input: 
        tz
    output:
        other.appTz
    */
    other.appTz = InterfaceToStr(query_map["tz"])
    if other.appTz == "" {
    //    log.Println("Parse tz fail:", err)
    }
    

    // $$ ========ue_pr ==============
    /* $$
    Leo: what if "ue_pr" doesn't exist?
    input: 
        ue_pr
    output:
        other.uepr_seqno
        other.uepr_action
        other.uepr_type
        other.duration
        other.scroll
        other.scontent 
        other.articleNid 
        other.memberid
        other.algorithmRaw
    */
    // ue_pr
    temp_uepr := InterfaceToStr(query_map["ue_pr"])
    if temp_uepr != "" {
        uepr := UePr{}
        if ueprRaw, err := url.QueryUnescape(temp_uepr) ; err == nil {
            if err = json.Unmarshal([]byte(ueprRaw), &uepr); err != nil {
                        log.Printf("Parse ue_pr (%s) fail: %v\n", ueprRaw, err)
                    }   
        }
        other.uepr_seqno = uepr.Data.Data.Seqno
        other.uepr_action = uepr.Data.Data.Action
        other.uepr_type = uepr.Data.Data.Type
        other.duration = uepr.Data.Data.Duration
        other.scroll = uepr.Data.Data.Scroll
        other.scontent = uepr.Data.Data.Scontent

        other.articleNid = "0"
        if len(uepr.Data.Data.Aid) > 0 {
            other.articleNid = uepr.Data.Data.Aid
        }
        other.memberid = "0"
        if len(uepr.Data.Data.Userid) > 0 {
            other.memberid = uepr.Data.Data.Userid
        }
        //devId := ""
        //if len(uepr.Data.Data.DevId) > 0 {
        //    devId = uepr.Data.Data.DevId
        //}
        other.algorithmRaw = uepr.Data.Data.Algorithm
    }
    


    // $$ ======== ==============
    /* $$
    input: 
        other.algorithmRaw
        other.aid
    output:
        other.channel_id
        other.score
        other.query_from_fmt
        other.query_to_fmt
        other.peo
        other.bias_schema
        other.bias_score
        other.bias_weight
        other.bias_value
        other.ur_weight
        other.lda_score
        other.lda_weight
        other.latest_score
        other.latest_weight
        other.ur_org
        other.name
        other.ur_score_str
        other.ur_scale_str
    */

    other.channel_id = "0"
    other.score = "0.0000"
    other.query_from_fmt = ""
    other.query_to_fmt = ""
    other.peo = "0"
    other.bias_schema = ""
    other.bias_score = "0.0000"
    other.bias_weight = "0.0000"
    other.bias_value = "0"
    ur_score := 0.0000
    other.ur_weight = "0.0000"
    ur_scale := 0.0000
    other.lda_score = "0.0000"
    other.lda_weight = "0.0000"
    other.latest_score = "0.0000"
    other.latest_weight = "0.0000"
    other.ur_org = "0.0000"
    other.name = ""

    if len(other.algorithmRaw)> 0 && other.aid == "StormNewsApp2"  {
        other.channel_id, other.score, other.query_from_fmt, other.query_to_fmt, other.bias_schema, other.peo, other.bias_score, other.bias_weight, other.bias_value, ur_score, other.ur_weight, ur_scale, other.lda_score, other.lda_weight, other.latest_score, other.latest_weight, other.name = ParseAlgorithm(other.algorithmRaw)
        other.ur_org = strconv.FormatFloat(float64(ur_score*ur_scale), 'f', 4, 64)
    }

    other.ur_score_str = strconv.FormatFloat(float64(ur_score), 'f', 4, 64)
    other.ur_scale_str = strconv.FormatFloat(float64(ur_scale), 'f', 4, 64)


    // $$ ======== res==============
    /* $$
    input: 
        res
    output:
        other.screen_w
        other.screen_h
    */

    //parse res to screen_w & screen_h
    other.screen_w = ""
    other.screen_h = ""
    res := InterfaceToStr(query_map["res"])
    if resValues := strings.Split(strings.ToLower(res), "x"); len(resValues) == 2 {
        other.screen_w = resValues[0]
        other.screen_h = resValues[1]
    }


    // $$ ========dtm==============
    /* $$
    input: 
        dtm
    output:
        other.dtm
        other.Db_app_dev_lagtime
    */
    //dtm ts to datetime
    var temp_dtm string = InterfaceToStr(query_map["dtm"])
    other.dtm = convert_unixtime_to_datetime(temp_dtm, "mili-sec")

    
    var temp_app_dev int64
    temp_app_dev, err = strconv.ParseInt(temp_dtm, 10, 64)
    temp_app_dev = temp_app_dev / 1000
    other.Db_app_dev_lagtime = strconv.FormatInt(get_currenttime_int64() - temp_app_dev, 10) // $$ get lag time in seconds

//    fmt.Println(get_currenttime_int64(), " - ", temp_app_dev, " = ",other.Db_app_dev_lagtime)
    
    // $$ ========lang==============
    /* $$
    input: 
        lang
    output:
        other.lang
    */
    // language
    other.lang, err = url.QueryUnescape(InterfaceToStr(query_map["lang"]))
    if err != nil {
        log.Printf("Parse language (%s) fail: %v\n", other.lang, err)
    }

    // $$ ========stm==============
    /* $$
    input: 
        stm
    output:
        other.stm
        other.Db_app_sent_lagtime
    */
    //stm
    var temp_stm string = InterfaceToStr(query_map["stm"])
    other.stm = convert_unixtime_to_datetime(temp_stm, "mili-sec")

     

    var temp_app_sent int64
    temp_app_sent, err = strconv.ParseInt(temp_stm, 10, 64)
    temp_app_sent = temp_app_sent / 1000
    other.Db_app_sent_lagtime = strconv.FormatInt(get_currenttime_int64() - temp_app_sent, 10) // $$ get lag time in seconds

   

    

    // $$ ========co==============
    /* $$
    input: 
        co
    output:
        other.sessionIndex = "0"
        other.sessionId = ""
        other.userId = ""
        other.prevSessionId = ""
        other.firstEventId = ""
        other.networkType = ""
        other.osType = ""
        other.carrier = ""
        other.osVer = ""
        other.model = ""
        other.manufacturer = ""
        other.devId
    */
    //co
    co := AppCo{}
    other.sessionIndex = "0"
    other.sessionId = ""
    other.userId = ""
    other.prevSessionId = ""
    other.firstEventId = ""
    other.networkType = ""
    other.osType = ""
    other.carrier = ""
    other.osVer = ""
    other.model = ""
    other.manufacturer = ""

    if coRaw, err := url.QueryUnescape(InterfaceToStr(query_map["co"])); err == nil {
        if err = json.Unmarshal([]byte(coRaw), &co); err != nil {
            log.Printf("Parse co (%s) fail: %v\n", coRaw, err)
        }
    }
    other.devId = ""
    for _, codata := range co.Data {
        //if aid == "StormNewsApp2" {
            //devId = uepr.Data.Data.DevId
    //}
        if len(codata.Data.NetType) > 0 {
            other.networkType = codata.Data.NetType
            other.osType = codata.Data.OsType
            other.osVer = codata.Data.OsVer
            other.carrier = codata.Data.Carrier
            other.manufacturer = codata.Data.Manufacturer
            other.model = codata.Data.Model
            if len(codata.Data.AppleId) > 0 {
                other.devId = codata.Data.AppleId
            }else {
                other.devId = codata.Data.AndroidId
            }
        } else if len(codata.Data.UserId) > 0 {
            other.prevSessionId = codata.Data.PrevSessionId
            other.sessionId = codata.Data.SessionId
            other.sessionIndex = strconv.Itoa(codata.Data.SessionIndex)
            other.userId = codata.Data.UserId
            other.firstEventId = codata.Data.FirstEventId 
        }
    }



    // $$ ========p==============
    /* $$
    input: 
        p
    output:
        other.platform
    */
    //get value
    other.platform = InterfaceToStr(query_map["p"])

    // $$ ========eid==============
    /* $$
    input: 
        eid
    output:
        other.eid
    */
    other.eid = InterfaceToStr(query_map["eid"])

    // $$ ========fp==============
    /* $$
    input: 
        fp
    output:
        other.fp
    */
    other.fp = InterfaceToStr(query_map["fp"])

    return other
}

func return_if_map_exist(input map[string]interface{}, key string) (output string) {
	output = ""
	if v, found := input[key]; found {
    //    fmt.Println(input, ": ", key, ": ", v)
        switch v.(type) {
        case string:
            output = InterfaceToStr(v)
        case []string:
            output = v.([]string)[0]
        }
	}
	return output
}


func convert_unixtime_to_time(input interface{}, format string) (output time.Time) {

    // $$ input: 1562137224094
    // $$ output: 2019-07-03 07:00:24.094 +0000 UTC
	var err error
	var unixtime_int64 int64
	
	switch input.(type) {
	case int64:
		unixtime_int64 = input.(int64)
	case int:
		unixtime_int64 = int64(input.(int))
	case string:
		unixtime_int64, err = strconv.ParseInt(input.(string), 10, 64)
	case float64:
		unixtime_int64 = int64(input.(float64))
	default:
		fmt.Println("InterFace type convert to int64 error,\n",input ,", ", err)
		unixtime_int64 = 0
	
	}	
	
	var exponent int64 // float64
	switch format {
	case "sec":
		exponent = 1000000000 // 9
	case "mili-sec":
		exponent = 1000000// 6
	case "nano-sec":
		exponent =  1// 0
	}
	
	nano_time_stamp := unixtime_int64 * exponent // int64(math.Pow(10, exponent))

	time_format := time.Unix(0, nano_time_stamp) // $$ convert nano-second to time format
//	fmt.Println("UnixTime_time: ", UnixTime_time, ", ", reflect.TypeOf(UnixTime_time))
	return time_format
}

func convert_unixtime_to_datetime (input interface{}, format string) (output string) {
    UnixTime_time := convert_unixtime_to_time(input, format)
    // $$ turn 1561715875951 into YYYY-MM-DD hh:mm:ss
    output = UnixTime_time.In(BOTz).Format(BO_TmFmt)
    return output
}

func type_assertion_map (input interface{}, prefix string, b *strings.Builder) {
    
}

func parseMap(aMap map[string]interface{}, prefix string, b *strings.Builder) {
	var sol string
	for key, val := range aMap {
        
		switch concreteVal := val.(type) {
		case map[string]interface{}:
			parseMap(concreteVal, prefix+key+".", b)
		case string:
			sol = key + "=" + concreteVal + " "
			b.WriteString(prefix)
			b.WriteString(sol)
			b.WriteRune(',')
		case float64:
			sol = key + "=" + strconv.FormatFloat(concreteVal, 'f', -1, 64) + " "
			b.WriteString(prefix)
			b.WriteString(sol)
            b.WriteRune(',')
        case int64:
            sol = key + "=" + strconv.FormatInt(concreteVal, 10) + " "
			b.WriteString(prefix)
			b.WriteString(sol)
            b.WriteRune(',')
        case []string:
        //    fmt.Println("into slice of stirng")
            temp := val.([]string)
            sol = key + "["
            for _, s := range temp {
                sol = sol + s + " "
            }
            sol = sol + "]"
            b.WriteString(prefix)
			b.WriteString(sol)
            b.WriteRune(',')
        case []interface{}:
        //    fmt.Println("into slice of interface{}")
            temp := val.([]interface{})
            sol = key + "["
            for _, s := range temp {
                sol = sol + InterfaceToStr(s) + " "
            }
            sol = sol + "]"
            b.WriteString(prefix)
			b.WriteString(sol)
            b.WriteRune(',')
		default:
            //What?
            fmt.Println("key: ", key, ": ", concreteVal)
            fmt.Println("val: ", val, ": ", reflect.TypeOf(concreteVal))
            fmt.Println(aMap)
            panic("Unsupported")
            
		}
	}
}
/*
func interface_2_string (input interface{}) (output string){
    switch input.(type) {
    case string:
        output = input.(string)
    default:
        panic("interface_2_string unsupported")
    }
    return output
}
*/
func parsecustom(aMap map[string]interface{}) string {
	b := &strings.Builder{}
	parseMap(aMap, "", b)

	return strings.TrimSuffix(b.String(), ",")
}


func get_currenttime_int64() (output int64) {

    var temp_current int64
    var err error
    current_unix_10 := strconv.FormatInt(time.Now().UTC().Unix(), 10) // $$ 10 digits(sec), ex: 1557998531388
    temp_current, err = strconv.ParseInt(current_unix_10, 10, 64)
    if err != nil {
        log.Printf("Get current time fail: %v\n", err)
    }
    return temp_current
}


func InterfaceToStr(value interface{}) string {

	switch value.(type) {
	case string:
        return value.(string)
    case []string:
        return value.([]string)[0]
	case int:
		return strconv.Itoa(value.(int))
	case int32:
		return strconv.Itoa(int(value.(int32)))
	case int64:
		return strconv.FormatInt(int64(value.(int64)), 10)
	case float64:
		return strconv.FormatFloat(value.(float64), 'f', -1, 64)
	case nil:
		return ""
	default:
		log.Println("InterFace type %T convert to string error,\n", value)
		return ""
	}
}