// package name: json2csv
package main

import (
    "bytes"
    "encoding/csv"
    "log"
    //"regexp"
    "strings"
    "syscall"
    "testing"
    "time"

    "github.com/oschwald/geoip2-golang"
    "github.com/ua-parser/uap-go/uaparser"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
)


type CDNLogTestSuite struct {
	suite.Suite
}

func (suite *CDNLogTestSuite) SetupTest() {
    var err error

    if BOTz, err = time.LoadLocation("Asia/Taipei"); err != nil {
        log.Fatalln("Load TW time zone fail:", err)
    }

    if GeoDB, err = geoip2.Open("../common/" + GEO_DB_NAME); err != nil {
        log.Fatalln("Open geo db fail:", err)
    }

    if UAParser, err = uaparser.New("../common/" + UA_PARSER_YAML); err != nil {
        log.Fatalln("New ua parser fail:", err)
    }

    // Compiled regexp
    //if len(CompiledReferRegexp) != len(ReReferSources) {
    //    log.Fatalln("Referer regexp structure mismatch")
    //}
    //for i, re := range ReReferSources {
    //    if CompiledReferRegexp[i], err = regexp.Compile(re.Clause); err != nil {
    //        log.Fatalf("Compile \"%s\" fail: %v\n", re.Clause, err)
    //    }
    //}
//
    //if len(CompiledUtmRegexp) != len(ReUtmSources) {
    //    log.Fatalln("Utm regexp structure mismatch")
    //}
    //for i, re := range ReUtmSources {
    //    if CompiledUtmRegexp[i], err = regexp.Compile(re.Clause); err != nil {
    //        log.Fatalf("Compile \"%s\" fail: %v\n", re.Clause, err)
    //    }
    //}


    OutDir = "/tmp"
}

func (suite *CDNLogTestSuite) Testlog() {
    var data = `2019-03-24	03:41:03	HKG51	693	2001:b400:e350:37ec:f0ba:bb2d:a787:904c	GET	d30cvjlci078zl.cloudfront.net	/i	200	-	TestProject/1%2520CFNetwork/976%2520Darwin/18.2.0	ue_pr=%257B%2522schema%2522%253A%2522iglu%253Acom.snowplowanalytics.snowplow%255C%252Funstruct_event%255C%252Fjsonschema%255C%252F1-0-0%2522%252C%2522data%2522%253A%257B%2522schema%2522%253A%2522iglu%253Amg.storm%255C%252Fcustom_context%255C%252Fjsonschema%255C%252F1-0-0%2522%252C%2522data%2522%253A%257B%2522action%2522%253A%2522%2522%252C%2522Latitude%2522%253A%252225.05660891974778%2522%252C%2522seqNo%2522%253A%2522%2522%252C%2522Longitude%2522%253A%2522121.5804200543689%2522%252C%2522aid%2522%253A%25221042585%2522%252C%2522type%2522%253A%2522readArticle%2522%252C%2522algorithm%2522%253A%2522%2522%257D%257D%257D&vp=640x1136&eid=d93249ab-9b75-4c8b-b227-2239e141b72f&p=mob&co=%257B%2522schema%2522%253A%2522iglu%253Acom.snowplowanalytics.snowplow%255C%252Fcontexts%255C%252Fjsonschema%255C%252F1-0-1%2522%252C%2522data%2522%253A%255B%257B%2522schema%2522%253A%2522iglu%253Acom.snowplowanalytics.snowplow%255C%252Fmobile_context%255C%252Fjsonschema%255C%252F1-0-1%2522%252C%2522data%2522%253A%257B%2522osType%2522%253A%2522ios%2522%252C%2522networkType%2522%253A%2522mobile%2522%252C%2522osVersion%2522%253A%252212.1.4%2522%252C%2522networkTechnology%2522%253A%2522CTRadioAccessTechnologyLTE%2522%252C%2522appleIdfv%2522%253A%25226DCD8831-B009-4611-B759-CF63F27CD4C4%2522%252C%2522carrier%2522%253A%2522%25E4%25B8%25AD%25E8%258F%25AF%25E9%259B%25BB%25E4%25BF%25A1%2522%252C%2522deviceManufacturer%2522%253A%2522Apple%2520Inc.%2522%252C%2522appleIdfa%2522%253A%25229250B21C-87C9-4145-B0B6-D848D68951C3%2522%252C%2522deviceModel%2522%253A%2522iPhone%2522%257D%257D%252C%257B%2522schema%2522%253A%2522iglu%253Acom.snowplowanalytics.snowplow%255C%252Fclient_session%255C%252Fjsonschema%255C%252F1-0-1%2522%252C%2522data%2522%253A%257B%2522previousSessionId%2522%253A%2522c46d8491-d158-4b16-bc75-65ba4d12a229%2522%252C%2522firstEventId%2522%253A%2522cbf9f9b8-620c-4576-b5c7-b8e0bcd4a528%2522%252C%2522sessionId%2522%253A%252228812491-adfc-4b9e-89bb-6953b8f41912%2522%252C%2522userId%2522%253A%2522368b6235-f078-4d82-9cf7-8b2c18bcd14d%2522%252C%2522sessionIndex%2522%253A378%252C%2522storageMechanism%2522%253A%2522SQLITE%2522%257D%257D%255D%257D&stm=1553398862920&dtm=1553398862910&tv=ios-0.6.2&tna=StormNewsAppNameSpace&e=ue&lang=zh-TW&aid=StormNewsApp&res=640x1136	smg_uid=1538806120374	Hit	rujKS2Sif1UrxfenKoYR9oGKd3wv_gbbkawP8TeZ9XnXFomnlHE_0A==	track.storm.mg	https	2079	0.365	-	TLSv1.2	ECDHE-RSA-AES128-GCM-SHA256	Hit	HTTP/1.1	-	-
`

    rSrc := csv.NewReader(strings.NewReader(data))
    rSrc.Comma = '\t'
    cdnlog, _ := rSrc.ReadAll()

    fdCdn := TranslateCdn(cdnlog)

    assert.NotEqual(suite.T(), fdCdn, -1)

    buf := make([]byte, 4096)
    syscall.Seek(fdCdn, 0, 0)
    nRead, err := syscall.Read(fdCdn, buf)
    assert.NotEqual(suite.T(), nRead, 0)
    assert.NoError(suite.T(), err)
    
    rTgt := csv.NewReader(bytes.NewReader(buf[:nRead]))
    rows, err := rTgt.ReadAll()
    assert.Equal(suite.T(), len(rows), 1)
    assert.Equal(suite.T(), len(rows[0]), 67)

    expected := []string{"1551663659", "showArticle", "android", "wifi", "7.0", "7e645537-361d-45f5-9ee9-778912927b18", "", "HTC", "HTC_A9u", "", "1a3bbb12-d95c-473b-9cf8-1fb1232814e8", "1cc7e7a2-904d-47b1-aeae-79017408ee03", "84046777-7139-4351-8e0f-cf0f14bf0cdf", "1", "2019-03-04 09:40:59", "2019-03-04 09:40:52", "中文", "1080", "1776", "2uREAbhy-bhcCzFkOb0nNiyPELStmtGIWuz_IicG_m_kVGz_Huc_1w==", "6", "{\"name\":\"home-new\",\"query_time\":\"2019-03-04T09:40:42+0800\"}", "", "", "", "0.0000", "", "StormNewsApp2", "", "", "", "", "0.0000", "0.0000", "0.0000", "0.0000", "0.0000", "0.0000", "0.0000", "0.0000", "", "mob", "0c0810c3-61af-40a8-9bf2-9373edcfd01c", "", "Asia/Taipei", "118.163.120.148", "", "0", "2019-03-04 09:40:59", "Other", "Other", "Other", "track.storm.mg", "unknown", "TW", "Taiwan", "23.5", "121", "Asia/Taipei", "1019012", "", "2019-03-04", "0", "", "", "", "home-new"}
    assert.Equal(suite.T(), expected, rows[0])
}


// TestCDNLogTestSuite invokes a normal testcase for cdnlog testsuite.
func TestCDNLogTestSuite(t *testing.T) {
	suite.Run(t, new(CDNLogTestSuite))
}
